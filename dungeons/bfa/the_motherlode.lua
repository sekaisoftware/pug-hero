-- get the namespace
local _, namespace = ...;

namespace.dungeons.bfa["The MOTHERLODE!!"] = {
	["Coin-Operated Crowd Pummeler"] =
	{
		["Normal"] = {
			"- Don't stand in front of the boss when he casts [Shocking Claw].",
			"- Kick the bombs into the boss.",
		},
		["Heroic"] = {
			"- Don't stand in front of the boss when he casts [Shocking Claw].",
			"- Kick the bombs into the boss.",
			"- Keep the boss away from piles of gold or he gets a buff."
		},
		["Mythic"] = {
			"- Don't stand in front of the boss when he casts [Shocking Claw].",
			"- Kick the bombs into the boss.",
			"- Keep the boss away from piles of gold or he gets a buff."
		}
	},
	["Azerokk"] =
	{
		["Normal"] = {
			"- Focus the add that is buffed (the one that's bigger than the others). Kill the adds before the boss.",
			"- Interrupt/Stun/Knockback the adds when they're casting [Resonant Quake].",
		},
		["Heroic"] = {
			"- Focus the add that is buffed (the one that's bigger than the others). Kill the adds before the boss.",
			"- Interrupt/Stun/Knockback the adds when they're casting [Resonant Quake].",
			"- Don't stand in front of the boss when he casts [Tectonic Smash]."
		},
		["Mythic"] = {
			"- Focus the add that is buffed (the one that's bigger than the others). Kill the adds before the boss.",
			"- Interrupt/Stun/Knockback the adds when they're casting [Resonant Quake].",
			"- Don't stand in front of the boss when he casts [Tectonic Smash]."
		}
	},
	["Rixxa Fluxflame"] =
	{
		["Normal"] = {
			"- Cleanse [Chemical Burn] off players as much as possible.",
			"- Don't stand in the pools of azerite.",
			"- Don't stand in [Propellant Blast]. Aim the blast at the pools of azerite on the ground when possible."
		},
		["Heroic"] = {
			"- Cleanse [Chemical Burn] off players as much as possible.",
			"- Don't stand in the pools of azerite.",
			"- Don't stand in [Propellant Blast]. Aim the blast at the pools of azerite on the ground when possible."
		},
		["Mythic"] = {
			"- Cleanse [Chemical Burn] off players as much as possible.",
			"- Don't stand in the pools of azerite.",
			"- Don't stand in [Propellant Blast]. Aim the blast at the pools of azerite on the ground when possible."
		}
	},
	["Mogul Razdunk"] =
	{
		["Normal"] = {
			"- Phase 1:",
			"- If targeted by [Homing Missle], run it out of the group.",
			"- Avoid getting hit by [Gatling Gun].",
			"- Phase 2 (When the boss hits 50% health):",
			"- Don't stand in the orange swirling circles and kill the adds.",
			"- When you're targeted by the boss, move to the rockets on the ground so that the boss smashes into them."
		},
		["Heroic"] = {
			"- Phase 1:",
			"- If targeted by [Homing Missle], run it out of the group.",
			"- Avoid getting hit by [Gatling Gun].",
			"- Phase 2 (When the boss hits 50% health):",
			"- Don't stand in the orange swirling circles and kill the adds.",
			"- When you're targeted by the boss, move to the rockets on the ground so that the boss smashes into them."
		},
		["Mythic"] = {
			"- Phase 1:",
			"- If targeted by [Homing Missle], run it out of the group.",
			"- Avoid getting hit by [Gatling Gun].",
			"- Phase 2 (When the boss hits 50% health):",
			"- Don't stand in the orange swirling circles and kill the adds.",
			"- When you're targeted by the boss, move to the rockets on the ground so that the boss smashes into them."
		}
	},
	
	
	
}