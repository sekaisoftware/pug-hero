-- get the namespace
local _, namespace = ...;

-- DOES NOT HAVE NORMAL
namespace.dungeons.bfa["Siege of Boralus"] = {
	["Chopper Redhook/Sergeant Bainbridge"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"- Prioritize killing the Irontide forces adds.",
			"- Run away when targeted by [On the Hook].",
			"- If the boss is chasing you, lead him into the bombs to detonate them.",
		},
		["Mythic"] = {
			"- Prioritize killing the Irontide forces adds.",
			"- Run away when targeted by [On the Hook].",
			"- If the boss is chasing you, lead him into the bombs to detonate them.",
		}
	},
	["Dread Captain Lockwood"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"- When the boss jumps to the ship, adds will come out.",
			"- Kill the adds, pick up the cannon they drop, and shoot it at the ship.",
		},
		["Mythic"] = {
			"- When the boss jumps to the ship, adds will come out.",
			"- Kill the adds, pick up the cannon they drop, and shoot it at the ship.",
		}
	},
	["Hadal Darkfathom"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"- Use the statue in the middle of the area to LOS [Tidal Surge]."
		},
		["Mythic"] = {
			"- Use the statue in the middle of the area to LOS [Tidal Surge]."
		}
	},
	["Viq'Goth"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"- Prioritize killing the Demolishing Terror tentacles and then kill the Gripping Terror so it releases the engineer. Then shoot the cannon at the boss. Do this 3 times to kill the boss.",
			"- Cleanse [Putrid Waters] as often as possible.",
			"- Stay out of the water."
		},
		["Mythic"] = {
			"- Prioritize killing the Demolishing Terror tentacles and then kill the Gripping Terror so it releases the engineer. Then shoot the cannon at the boss. Do this 3 times to kill the boss.",
			"- Cleanse [Putrid Waters] as often as possible.",
			"- Stay out of the water."
		}
	},
	
	
	
}