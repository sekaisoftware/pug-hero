-- get the namespace
local _, namespace = ...;

namespace.dungeons.bfa["Freehold"] = {
	["Skycap'n Kragg"] =
	{
		["Normal"] = {
			"- Avoid the green stuff on the ground.",
			"- Interrupt [Revitalizing Brew].",
			"- Spread out for [Azerite Powder Shot]."
		},
		["Heroic"] = {
			"- Avoid the green stuff on the ground.",
			"- Don't get hit by the parrot when it's charging.",
			"- Interrupt [Revitalizing Brew].",
			"- Spread out for [Azerite Powder Shot]."
		},
		["Mythic"] = {
			"- Avoid the green stuff on the ground.",
			"- Don't get hit by the parrot when it's charging.",
			"- Interrupt [Revitalizing Brew].",
			"- Spread out for [Azerite Powder Shot]."
		}
	},
	["Council o' Captains"] =
	{
		["Normal"] = {
			"- Free allies in [Blackout Barrel] by damaging the barrel.",
			"- Move behind Eudora when he disengages away to avoid his shots.",
			"- Let the spinnig blades go back to the boss so they go away.",
			"- Prioritize killing them in this order: Eudora -> Jolly -> Roul.",
			"- Note: By doing the pre-event, we'll only fight two of the three captians."
		},
		["Heroic"] = {
			"- Free allies in [Blackout Barrel] by damaging the barrel.",
			"- Move behind Eudora when he disengages away to avoid his shots.",
			"- Let the spinnig blades go back to the boss so they go away.",
			"- Prioritize killing them in this order: Eudora -> Jolly -> Roul.",
			"- Note: By doing the pre-event, we'll only fight two of the three captians."
		},
		["Mythic"] = {
			"- Free allies in [Blackout Barrel] by damaging the barrel.",
			"- Move behind Eudora when he disengages away to avoid his shots.",
			"- Let the spinnig blades go back to the boss so they go away.",
			"- Prioritize killing them in this order: Eudora -> Jolly -> Roul.",
			"- Note: By doing the pre-event, we'll only fight two of the three captians."
		}
	},
	["Ring of Booty"] =
	{
		["Normal"] = {
			"- Phase 1: Spam click the bone pile to catch the pig.",
			"- Phase 2: Avoid turtle shells.",
			"- Phase 3: Kite sharks and get away from the boss when he starts spinning."
		},
		["Heroic"] = {
			"- Phase 1: Spam click the bone pile to catch the pig.",
			"- Phase 2: Avoid turtle shells.",
			"- Phase 3: Kite sharks through the red stuff on the ground to slow them down and get away from the boss when he starts spinning."
		},
		["Mythic"] = {
			"- Phase 1: Spam click the bone pile to catch the pig.",
			"- Phase 2: Avoid turtle shells.",
			"- Phase 3: Kite sharks through the red stuff on the ground to slow them down and get away from the boss when he starts spinning."
		}
	},
	["Harlan Sweete"] =
	{
		["Normal"] = {
			"- Kill Irontide Gernaiders before they reach their targets. They can be stunned/slowed/rooted",
			"- Don't stand in the fire and don't run the cannon bombardment through the group."
		},
		["Heroic"] = {
			"- Kill Irontide Gernaiders before they reach their targets. They can be stunned/slowed/rooted",
			"- Don't stand in the fire and don't run the cannon bombardment through the group."
		},
		["Mythic"] = {
			"- Kill Irontide Gernaiders before they reach their targets. They can be stunned/slowed/rooted",
			"- Don't stand in the fire and don't run the cannon bombardment through the group."
		}
	}
}