-- get the namespace
local _, namespace = ...;

-- DOES NOT HAVE NORMAL
namespace.dungeons.bfa["Kings' Rest"] = {
	["The Golden Serpent"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"- If you're targeted with [Spit Gold], drop the debuff away from the group. Drop them as close together as possible.",
			"- After 3 [Spit Gold] casts, the gold dropped will turn into adds. DPS down the adds before they reach the boss or slow/root them while we finish off the boss."
		},
		["Mythic"] = {
			"- If you're targeted with [Spit Gold], drop the debuff away from the group. Drop them as close together as possible.",
			"- After 3 [Spit Gold] casts, the gold dropped will turn into adds. DPS down the adds before they reach the boss or slow/root them while we finish off the boss."
		}
	},
	["Mchimba the Embalmer"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"- Don't stand in the fire.",
			"- Heal players who have just been targeted by [Drain Fluids] above 90% health to remove the debuff.",
			"- The boss randomly will put a player in one of the coffins at the edge of the room. Everyone else needs to go to that coffin and free the player. The player can shake the coffin."
		},
		["Mythic"] = {
			"- Don't stand in the fire.",
			"- Heal players who have just been targeted by [Drain Fluids] above 90% health to remove the debuff.",
			"- The boss randomly will put a player in one of the coffins at the edge of the room. Everyone else needs to go to that coffin and free the player. The player can shake the coffin."
		}
	},
	["The Council of Tribes"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"- This fight has 3 bosses we fight one at a time. The order in which we fight them changes from week to week.",
			"- Kula the Butcher: Dodge whirling axes.",
			"- Aka'ali: Stand between the boss and his target when he's casting [Barrel Through] to split the damage. Tank: kite the boss after he hits you with [Debilitating Backhand] until the [Shattered Defenses] debuff you get goes away.",
			"- Zanazal the Wise: ALWAYS intterupt [Poison Nova] or it will one-shot the group. Interrupt Lightning Bolt as much as possible. DPS down totems in the following order: Explosive Totem > Thundering Totem > Torrent Totem > Earthwall Totem.",
			"- The mechanics of the previous bosses killed carry on into the next boss. So watch out for previous mechanics on top of the current boss we're fighting."
		},
		["Mythic"] = {
			"- This fight has 3 bosses we fight one at a time. The order in which we fight them changes from week to week.",
			"- Kula the Butcher: Dodge whirling axes.",
			"- Aka'ali: Stand between the boss and his target when he's casting [Barrel Through] to split the damage. Tank: kite the boss after he hits you with [Debilitating Backhand] until the [Shattered Defenses] debuff you get goes away.",
			"- Zanazal the Wise: ALWAYS intterupt [Poison Nova] or it will one-shot the group. Interrupt Lightning Bolt as much as possible. DPS down totems in the following order: Explosive Totem > Thundering Totem > Torrent Totem > Earthwall Totem.",
			"- The mechanics of the previous bosses killed carry on into the next boss. So watch out for previous mechanics on top of the current boss we're fighting."
		}
	},
	["Dazar, The First King"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"- When targeted with [Quaking Leap], run out of the group.",
			"- Dodge the whirlwinds.",
			"- Kill the adds he summons (one at 80% health, the other at 60% health).",
			"- Avoid the spears that drop from the ceiling (starts when the boss hits 40% health)."
		},
		["Mythic"] = {
			"- When targeted with [Quaking Leap], run out of the group.",
			"- Dodge the whirlwinds.",
			"- Kill the adds he summons (one at 80% health, the other at 60% health).",
			"- Avoid the spears that drop from the ceiling (starts when the boss hits 40% health)."
		}
	},
	
	
	
}