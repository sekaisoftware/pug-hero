-- get the namespace
local _, namespace = ...;

namespace.dungeons.shadowlands["De Other Side"] = {
	["Hakkar the Soulflayer"] =
	{
		["Normal"] = {
			"- If targeted with [322746] (red circle), keep that circle away from other players.",
			"- Prioritize killing adds. Don't stand in the pool they drop when they die.",
			"- Dodge the red swirls on the ground."
		},
		["Heroic"] = {
			"- If targeted with [322746] (red circle), keep that circle away from other players.",
			"- Prioritize killing adds. Don't stand in the pool they drop when they die.",
			"- Dodge the red swirls on the ground."
		},
		["Mythic"] = {
			"- If targeted with [322746] (red circle), keep that circle away from other players.",
			"- Prioritize killing adds. Don't stand in the pool they drop when they die.",
			"- Dodge the red swirls on the ground."
		}
	},
	["The Manastorms"] =
	{
		["Normal"] = {
			"- Intercept the beams coming from the crystals. When the damage from the beam gets too high, rotate out with another player. Wait until the debuff drops before intercepting another beam.",
			"- Disarm the bombs that run away from the boss by clicking on them."
		},
		["Heroic"] = {
			"- Intercept the beams coming from the crystals. When the damage from the beam gets too high, rotate out with another player. Wait until the debuff drops before intercepting another beam.",
			"- Disarm the bombs that run away from the boss by clicking on them."
		},
		["Mythic"] = {
			"- Intercept the beams coming from the crystals. When the damage from the beam gets too high, rotate out with another player. Wait until the debuff drops before intercepting another beam.",
			"- Disarm the bombs that run away from the boss by clicking on them."
		}
	},
	["Dealer Xy'exa"] =
	{
		["Normal"] = {
			"- If you get targeted with a bomb timer, step on the white traps to be launched into the air right before the bomb explodes.",
			"- When the boss casts [320232], step onto a trap to be launched into the air right before the cast finishes to avoid it."
		},
		["Heroic"] = {
			"- If you get targeted with a bomb timer, step on the white traps to be launched into the air right before the bomb explodes.",
			"- When the boss casts [320232], step onto a trap to be launched into the air right before the cast finishes to avoid it."
		},
		["Mythic"] = {
			"- If you get targeted with a bomb timer, step on the white traps to be launched into the air right before the bomb explodes.",
			"- When the boss casts [320232], step onto a trap to be launched into the air right before the cast finishes to avoid it."
		}
	},
	["Mueh'zala"] =
	{
		["Normal"] = {
			"- When the boss casts [325258], look at his arms to tell which way you need to dodge. (left, right, or backwards).",
			"- During [326171] (portals), 2 DPS should go left and the rest of the group should go right. One player per portal. Kill the add and then click the totem to get back.",
			"- Healer: Dispell debuffs when possible.",
			"- Avoid the purple circles on the ground that spawn when the debuff is removed or expires."
		},
		["Heroic"] = {
			"- When the boss casts [325258], look at his arms to tell which way you need to dodge. (left, right, or backwards).",
			"- During [326171] (portals), 2 DPS should go left and the rest of the group should go right. One player per portal. Kill the add and then click the totem to get back.",
			"- Healer: Dispell debuffs when possible.",
			"- Avoid the purple circles on the ground that spawn when the debuff is removed or expires."
		},
		["Mythic"] = {
			"- When the boss casts [325258], look at his arms to tell which way you need to dodge. (left, right, or backwards).",
			"- During [326171] (portals), 2 DPS should go left and the rest of the group should go right. One player per portal. Kill the add and then click the totem to get back.",
			"- Healer: Dispell debuffs when possible.",
			"- Avoid the purple circles on the ground that spawn when the debuff is removed or expires."
		}
	}
}