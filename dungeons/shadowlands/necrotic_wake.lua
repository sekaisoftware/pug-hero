-- get the namespace
local _, namespace = ...;

namespace.dungeons.shadowlands["The Necrotic Wake"] = {
	["Brightbone"] =
	{
		["Normal"] = {
			"- Prioritize killing worms. When fixated by a worm, keep your distance.",
			"- Don't stand in the green pools.",
		},
		["Heroic"] = {
			"- Prioritize killing worms. When fixated by a worm, keep your distance.",
			"- Don't stand in the green pools.",
		},
		["Mythic"] = {
			"- Prioritize killing worms. When fixated by a worm, keep your distance.",
			"- Don't stand in the green pools.",
		}
	},
	["Amarth, The Harvester"] =
	{
		["Normal"] = {
			"- Avoid the swirls on the ground.",
			"- Focus down and interrupt the adds. When the adds die, they will explode. Don't stand in the explosion.",
		},
		["Heroic"] = {
			"- Avoid the swirls on the ground.",
			"- Focus down and interrupt the adds. When the adds die, they will explode. Don't stand in the explosion.",
		},
		["Mythic"] = {
			"- Avoid the swirls on the ground.",
			"- Focus down and interrupt the adds. When the adds die, they will explode. Don't stand in the explosion.",
		}
	},
	["Surgeon Stitchflesh"] =
	{
		["Normal"] = {
			"- Tank the add somewhat close to the platform Stitchflesh is on, starting slightly off to the side.",
			"- Stack on the add and move as a group to avoid the green swirls.",
			"- When targeted by [322681], aim the hook at Stitchflesh (on the platform) and then dodge the hook.",
			"- When the boss is pulled off the platform, DPS him until he returns. When he returns, a new add will spawn.",
		},
		["Heroic"] = {
			"- Tank the add somewhat close to the platform Stitchflesh is on, starting slightly off to the side.",
			"- Stack on the add and move as a group to avoid the green swirls.",
			"- When targeted by [322681], aim the hook at Stitchflesh (on the platform) and then dodge the hook.",
			"- When the boss is pulled off the platform, DPS him until he returns. When he returns, a new add will spawn.",
		},
		["Mythic"] = {
			"- Tank the add somewhat close to the platform Stitchflesh is on, starting slightly off to the side.",
			"- Stack on the add and move as a group to avoid the green swirls.",
			"- When targeted by [322681], aim the hook at Stitchflesh (on the platform) and then dodge the hook.",
			"- When the boss is pulled off the platform, DPS him until he returns. When he returns, a new add will spawn.",
		}
	},
	["Nalthor the Rimebinder"] =
	{
		["Normal"] = {
			"- Avoid the swirls on the ground.",
			"- A player will be rooted randomly. Move away from that player and then dispel the root.",
			"- A DPS will be randomly sent to another platform. That player needs to run to the end of the platform and kill the add at the end.",
		},
		["Heroic"] = {
			"- Avoid the swirls on the ground.",
			"- A player will be rooted randomly. Move away from that player and then dispel the root.",
			"- A DPS will be randomly sent to another platform. That player needs to run to the end of the platform and kill the add at the end.",
		},
		["Mythic"] = {
			"- Avoid the swirls on the ground.",
			"- A player will be rooted randomly. Move away from that player and then dispel the root.",
			"- A DPS will be randomly sent to another platform. That player needs to run to the end of the platform and kill the add at the end.",
		}
	}
}