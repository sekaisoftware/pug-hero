-- get the namespace
local _, namespace = ...;

namespace.dungeons.shadowlands["Spires of Ascension"] = {
	["Kin-Tara"] =
	{
		["Normal"] = {
			"- Tank: Face the boss away from the group.",
			"- Interrupt [327481].",
			"- When the boss flies into the air, spread out. Whoever is targeted by [321009] needs to get out of the pool it leaves behind.",
			"- The boss and pet need to be killed around the same time to avoid them enraging.",
		},
		["Heroic"] = {
			"- Tank: Face the boss away from the group.",
			"- Interrupt [327481].",
			"- When the boss flies into the air, spread out. Whoever is targeted by [321009] needs to get out of the pool it leaves behind.",
			"- The boss and pet need to be killed around the same time to avoid them enraging.",
		},
		["Mythic"] = {
			"- Tank: Face the boss away from the group.",
			"- Interrupt [327481].",
			"- When the boss flies into the air, spread out. Whoever is targeted by [321009] needs to get out of the pool it leaves behind.",
			"- The boss and pet need to be killed around the same time to avoid them enraging.",
		}
	},
	["Ventunax"] =
	{
		["Normal"] = {
			"- Tank: Face the boss away from the rest of the group.",
			"- Dodge the dark bolts that come out of the dark cloud or you'll be shot into the air.",
		},
		["Heroic"] = {
			"- Tank: Face the boss away from the rest of the group.",
			"- Dodge the dark bolts that come out of the dark cloud or you'll be shot into the air.",
		},
		["Mythic"] = {
			"- Tank: Face the boss away from the rest of the group.",
			"- Dodge the dark bolts that come out of the dark cloud or you'll be shot into the air.",
		}
	},
	["Oryphrion"] =
	{
		["Normal"] = {
			"- Spread out for the fight.",
			"- Healer: Dispel the tank whenever the boss casts [324608]",
			"- When the boss casts [324444] each player drops a blue circle. Get out of the blue circles and move the boss out of the blue circles as well. Orbs spawn from those circles which need to be soaked before they reach the boss.",
		},
		["Heroic"] = {
			"- Spread out for the fight.",
			"- Healer: Dispel the tank whenever the boss casts [324608]",
			"- When the boss casts [324444] each player drops a blue circle. Get out of the blue circles and move the boss out of the blue circles as well. Orbs spawn from those circles which need to be soaked before they reach the boss.",
		},
		["Mythic"] = {
			"- Spread out for the fight.",
			"- Healer: Dispel the tank whenever the boss casts [324608]",
			"- When the boss casts [324444] each player drops a blue circle. Get out of the blue circles and move the boss out of the blue circles as well. Orbs spawn from those circles which need to be soaked before they reach the boss.",
		}
	},
	["Devos, Paragon of Doubt"] =
	{
		["Normal"] = {
			"- Stand somewhat near the edge of the platform for the fight to keep the bosses charge distance shorter.",
			"- Get away from the orbs that spawn from the swirls.",
			"- When targeted with [322817], run away from the rest of the group. The healer should then dispel it.",
			"- After the boss activates the anima conduit, pick up the blue orbs that spawn and bring them to the anima conduit in the middle. Once all the orbs are collected, someone needs to click the anima conduit and target the boss to throw a spear at the boss.",
		},
		["Heroic"] = {
			"- Stand somewhat near the edge of the platform for the fight to keep the bosses charge distance shorter.",
			"- Get away from the orbs that spawn from the swirls.",
			"- When targeted with [322817], run away from the rest of the group. The healer should then dispel it.",
			"- After the boss activates the anima conduit, pick up the blue orbs that spawn and bring them to the anima conduit in the middle. Once all the orbs are collected, someone needs to click the anima conduit and target the boss to throw a spear at the boss.",
		},
		["Mythic"] = {
			"- Stand somewhat near the edge of the platform for the fight to keep the bosses charge distance shorter.",
			"- Get away from the orbs that spawn from the swirls.",
			"- When targeted with [322817], run away from the rest of the group. The healer should then dispel it.",
			"- After the boss activates the anima conduit, pick up the blue orbs that spawn and bring them to the anima conduit in the middle. Once all the orbs are collected, someone needs to click the anima conduit and target the boss to throw a spear at the boss.",
		}
	}
}