-- get the namespace
local _, namespace = ...;

namespace.dungeons.shadowlands["Mists of Tirna Scithe"] = {
	["Ingra Maloch"] =
	{
		["Normal"] = {
			"- Tank: Face the tree add away from the group.",
			"- Kill the tree add to apply a debuff on the boss, then switch to the boss.",
			"- Stop attacking Maloch after he casts [325697] on himself.",
			"- Avoid the swirls on the ground.",
		},
		["Heroic"] = {
			"- Tank: Face the tree add away from the group.",
			"- Kill the tree add to apply a debuff on the boss, then switch to the boss.",
			"- Stop attacking Maloch after he casts [325697] on himself.",
			"- Avoid the swirls on the ground.",
		},
		["Mythic"] = {
			"- Tank: Face the tree add away from the group.",
			"- Kill the tree add to apply a debuff on the boss, then switch to the boss.",
			"- Stop attacking Maloch after he casts [325697] on himself.",
			"- Avoid the swirls on the ground.",
		}
	},
	["Mistcaller"] =
	{
		["Normal"] = {
			"- Tank: Interrupt [321828]. Ony tanks can interrupt it.",
			"- The boss will cast [341709] on a player which will cause a fox to fixate them. Everyone needs to avoid the fox.",
			"- During [321471], kill the add with a dissimilar icon above their head. It's just like the maze puzzle leading up to the boss.",
		},
		["Heroic"] = {
			"- Tank: Interrupt [321828]. Ony tanks can interrupt it.",
			"- The boss will cast [341709] on a player which will cause a fox to fixate them. Everyone needs to avoid the fox.",
			"- During [321471], kill the add with a dissimilar icon above their head. It's just like the maze puzzle leading up to the boss.",
		},
		["Mythic"] = {
			"- Tank: Interrupt [321828]. Ony tanks can interrupt it.",
			"- The boss will cast [341709] on a player which will cause a fox to fixate them. Everyone needs to avoid the fox.",
			"- During [321471], kill the add with a dissimilar icon above their head. It's just like the maze puzzle leading up to the boss.",
		}
	},
	["Tred'ova"] =
	{
		["Normal"] = {
			"- Avoid the green and blue swirls on the ground.",
			"- Prioritize killing the bug adds.",
			"- When targeted with [322614], run away from the other player you're linked with until it breaks.",
		},
		["Heroic"] = {
			"- Avoid the green and blue swirls on the ground.",
			"- Prioritize killing the bug adds.",
			"- When targeted with [322614], run away from the other player you're linked with until it breaks.",
		},
		["Mythic"] = {
			"- Avoid the green and blue swirls on the ground.",
			"- Prioritize killing the bug adds.",
			"- When targeted with [322614], run away from the other player you're linked with until it breaks.",
		}
	}
}