-- get the namespace
local _, namespace = ...;

namespace.dungeons.shadowlands["Halls of Atonement"] = {
	["Halkias, the Sin-Stained Goliath"] =
	{
		["Normal"] = {
			"- Fill up the Halkias' Anima bar by killing 3 Shard of Halkias adds found around the area.",
			"- Don't stand in the patches he spawns on the ground and if possible try to stack them in a way they'll be easy to avoid.",
			"- Avoid the laser beams. They can change direction while turning, so be ready for that.",
			"- Dispel players that are feared.",
		},
		["Heroic"] = {
			"- Fill up the Halkias' Anima bar by killing 3 Shard of Halkias adds found around the area.",
			"- Don't stand in the patches he spawns on the ground and if possible try to stack them in a way they'll be easy to avoid.",
			"- Avoid the laser beams. They can change direction while turning, so be ready for that.",
			"- Dispel players that are feared.",
		},
		["Mythic"] = {
			"- Fill up the Halkias' Anima bar by killing 3 Shard of Halkias adds found around the area.",
			"- Don't stand in the patches he spawns on the ground and if possible try to stack them in a way they'll be easy to avoid.",
			"- Avoid the laser beams. They can change direction while turning, so be ready for that.",
			"- Dispel players that are feared.",
		}
	},
	["Echelon"] =
	{
		["Normal"] = {
			"- Avoid the red pools.",
			"- Try to kill the adds in roughly the same spot. When targeted with [319592], run on top of the dead adds so the boss leaps onto them.",
			"- Adds explode when killed. If the damage is too much to keep up with, consider killing them one at a time.",
		},
		["Heroic"] = {
			"- Avoid the red pools.",
			"- Try to kill the adds in roughly the same spot. When targeted with [319592], run on top of the dead adds so the boss leaps onto them.",
			"- Adds explode when killed. If the damage is too much to keep up with, consider killing them one at a time.",
		},
		["Mythic"] = {
			"- Avoid the red pools.",
			"- Try to kill the adds in roughly the same spot. When targeted with [319592], run on top of the dead adds so the boss leaps onto them.",
			"- Adds explode when killed. If the damage is too much to keep up with, consider killing them one at a time.",
		}
	},
	["High Adjudicator Aleez"] =
	{
		["Normal"] = {
			"- Interrupt [323538] and [323552]. If your group is low on interrupts, prioritize [323552].",
			"- If fixated by the add, lead the add to one of the red vessels nearby. The add will stop moving if the fixated player looks at it.",
			"- Avoid the big red swirls on the ground.",
		},
		["Heroic"] = {
			"- Interrupt [323538] and [323552]. If your group is low on interrupts, prioritize [323552].",
			"- If fixated by the add, lead the add to one of the red vessels nearby. The add will stop moving if the fixated player looks at it.",
			"- Avoid the big red swirls on the ground.",
		},
		["Mythic"] = {
			"- Interrupt [323538] and [323552]. If your group is low on interrupts, prioritize [323552].",
			"- If fixated by the add, lead the add to one of the red vessels nearby. The add will stop moving if the fixated player looks at it.",
			"- Avoid the big red swirls on the ground.",
		}
	},
	["Lord Chamberlain"] =
	{
		["Normal"] = {
			"- Tanks: use defensive cooldowns during [323437].",
			"- Avoid being hit by the statues.",
			"- When the boss shoots red beams at the statues, run away from the boss. After the boss pulls the statues in, he's going to push them away. Move inbetween the statues to avoid being hit.",
			"- Someone in the group (usually the Tank) needs to intercept the red bolts during [Ritual of Woe].",
		},
		["Heroic"] = {
			"- Tanks: use defensive cooldowns during [323437].",
			"- Avoid being hit by the statues.",
			"- When the boss shoots red beams at the statues, run away from the boss. After the boss pulls the statues in, he's going to push them away. Move inbetween the statues to avoid being hit.",
			"- Someone in the group (usually the Tank) needs to intercept the red bolts during [Ritual of Woe].",
		},
		["Mythic"] = {
			"- Tanks: use defensive cooldowns during [323437].",
			"- Avoid being hit by the statues.",
			"- When the boss shoots red beams at the statues, run away from the boss. After the boss pulls the statues in, he's going to push them away. Move inbetween the statues to avoid being hit.",
			"- Someone in the group (usually the Tank) needs to intercept the red bolts during [Ritual of Woe].",
		}
	}
}