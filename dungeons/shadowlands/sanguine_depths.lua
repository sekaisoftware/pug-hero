-- get the namespace
local _, namespace = ...;

namespace.dungeons.shadowlands["Sanguine Depths"] = {
	["Kryxis the Voracious"] =
	{
		["Normal"] = {
			"- Tank: Don't face the boss towards the edge or you'll get knocked off.",
			"- Interrupt [319654].",
			"- When a player is targeted with [319713] the rest of the group should move between the boss and the player to reduce damage.",
			"- Soak the orbs that spawn after the boss uses [319685] before they reach the boss.",
		},
		["Heroic"] = {
			"- Tank: Don't face the boss towards the edge or you'll get knocked off.",
			"- Interrupt [319654].",
			"- When a player is targeted with [319713] the rest of the group should move between the boss and the player to reduce damage.",
			"- Soak the orbs that spawn after the boss uses [319685] before they reach the boss.",
		},
		["Mythic"] = {
			"- Tank: Don't face the boss towards the edge or you'll get knocked off.",
			"- Interrupt [319654].",
			"- When a player is targeted with [319713] the rest of the group should move between the boss and the player to reduce damage.",
			"- Soak the orbs that spawn after the boss uses [319685] before they reach the boss.",
		}
	},
	["Executor Tarvold"] =
	{
		["Normal"] = {
			"- Stay spread out for the fight.",
			"- Kill the add quickly and don't stand in the red pool the add leaves behind.",
		},
		["Heroic"] = {
			"- Stay spread out for the fight.",
			"- Kill the add quickly and don't stand in the red pool the add leaves behind.",
		},
		["Mythic"] = {
			"- Stay spread out for the fight.",
			"- Kill the add quickly and don't stand in the red pool the add leaves behind.",
		}
	},
	["Grand Proctor Beryllia"] =
	{
		["Normal"] = {
			"- During [325360], stand on the bright yellow circle to reduce damage.",
			"- Avoid the swirls on the ground.",
		},
		["Heroic"] = {
			"- During [325360], stand on the bright yellow circle to reduce damage.",
			"- Avoid the swirls on the ground.",
		},
		["Mythic"] = {
			"- During [325360], stand on the bright yellow circle to reduce damage.",
			"- Avoid the swirls on the ground.",
		}
	},
	["General Kaal"] =
	{
		["Normal"] = {
			"- Before pulling the boss, one player should pick up the Essence of Z'rali (yellow thing) that will grant them an extra ability.",
			"- Stay spread out for the majority of the fight.",
			"- Avoid the red beam that happens across the platform.",
			"- When [322895] is being cast, the person with the extra action ability should use the ability and everyone else needs to stack inside of it.",
		},
		["Heroic"] = {
			"- Before pulling the boss, one player should pick up the Essence of Z'rali (yellow thing) that will grant them an extra ability.",
			"- Stay spread out for the majority of the fight.",
			"- Avoid the red beam that happens across the platform.",
			"- When [322895] is being cast, the person with the extra action ability should use the ability and everyone else needs to stack inside of it.",
		},
		["Mythic"] = {
			"- Before pulling the boss, one player should pick up the Essence of Z'rali (yellow thing) that will grant them an extra ability.",
			"- Stay spread out for the majority of the fight.",
			"- Avoid the red beam that happens across the platform.",
			"- When [322895] is being cast, the person with the extra action ability should use the ability and everyone else needs to stack inside of it.",
		}
	}
}