-- get the namespace
local _, namespace = ...;

namespace.dungeons.shadowlands["Plaguefall"] = {
	["Globgrog"] =
	{
		["Normal"] = {
			"- Cleanse [331238] whenever possible.",
			"- Avoid [324667] the best you can.",
			"- During [327584], move the boss to the opposite end of the room that the 3 slimes spawn. Kill the slimes before they reach the boss.",
		},
		["Heroic"] = {
			"- Cleanse [331238] whenever possible.",
			"- Avoid [324667] the best you can.",
			"- During [327584], move the boss to the opposite end of the room that the 3 slimes spawn. Kill the slimes before they reach the boss.",
		},
		["Mythic"] = {
			"- Cleanse [331238] whenever possible.",
			"- Avoid [324667] the best you can.",
			"- During [327584], move the boss to the opposite end of the room that the 3 slimes spawn. Kill the slimes before they reach the boss.",
		}
	},
	["Doctor Ickus"] =
	{
		["Normal"] = {
			"- Tank needs to always be in melee range of the boss.",
			"- Dispel [329110] from the tank when possible. This will spawn an add. Prioritize killing the add when spawned.",
			"- Interrupt and kill the red slime adds. Stand in the red pools they drop for a haste buff. Keep the boss out of the red pools.",
			"- When the boss jumps to a new platform, prioritize killing the Plague Bomb on that platform.",
		},
		["Heroic"] = {
			"- Tank needs to always be in melee range of the boss.",
			"- Dispel [329110] from the tank when possible. This will spawn an add. Prioritize killing the add when spawned.",
			"- Interrupt and kill the red slime adds. Stand in the red pools they drop for a haste buff. Keep the boss out of the red pools.",
			"- When the boss jumps to a new platform, prioritize killing the Plague Bomb on that platform.",
		},
		["Mythic"] = {
			"- Tank needs to always be in melee range of the boss.",
			"- Dispel [329110] from the tank when possible. This will spawn an add. Prioritize killing the add when spawned.",
			"- Interrupt and kill the red slime adds. Stand in the red pools they drop for a haste buff. Keep the boss out of the red pools.",
			"- When the boss jumps to a new platform, prioritize killing the Plague Bomb on that platform.",
		}
	},
	["Domina Venomblade"] =
	{
		["Normal"] = {
			"- Look for the webs on the ground. Stand on them to bring out spider adds and focus them down.",
			"- Get away from others if targeted with [325245].",
		},
		["Heroic"] = {
			"- Look for the webs on the ground. Stand on them to bring out spider adds and focus them down.",
			"- Get away from others if targeted with [325245].",
		},
		["Mythic"] = {
			"- Look for the webs on the ground. Stand on them to bring out spider adds and focus them down.",
			"- Get away from others if targeted with [325245].",
		}
	},
	["Margrave Stradama"] =
	{
		["Normal"] = {
			"- Tank: Stand inside the green circle on the ground when the add spawns.",
			"- Prioritize killing the add.",
			"- When the tentacles spawn, dodge them by moving to an opening between them.",
		},
		["Heroic"] = {
			"- Tank: Stand inside the green circle on the ground when the add spawns.",
			"- Prioritize killing the add.",
			"- When the tentacles spawn, dodge them by moving to an opening between them.",
		},
		["Mythic"] = {
			"- Tank: Stand inside the green circle on the ground when the add spawns.",
			"- Prioritize killing the add.",
			"- When the tentacles spawn, dodge them by moving to an opening between them.",
		}
	}
}