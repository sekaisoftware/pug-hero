-- get the namespace
local _, namespace = ...;

namespace.dungeons.shadowlands["Tazavesh the Veiled Market"] = {
	["Zo'phex the Sentinel"] =
	{
		["Normal"] = {
			"This fight currently doesn't have a description."
		},
		["Heroic"] = {
			"This fight currently doesn't have a description."
		},
		["Mythic"] = {
			"- If targeted by [345598] (red arrow), run away from the boss.",
			"- The person targeted will get stunned/rooted in a cage. That person needs to be broken out before the boss reaches them.",
			"- Get out of the boss's path towrads the cage.",
			"- Stay out of the spinning blades and move around the shield he puts up ([347992])."
		}
	},
	["The Grand Menagerie"] =
	{
		["Normal"] = {
			"This fight currently doesn't have a description."
		},
		["Heroic"] = {
			"This fight currently doesn't have a description."
		},
		["Mythic"] = {
			"- Three bosses in total. The next one comes out 30 seconds after the current one hits 40%. Finish off the one that hit 40% first before moving on to the next.",
			"- FIRST BOSS: Alcruux",
			"- If you are targeted with [349627], stay at least 5 yards away from other players. You can absorb anima orbs for a stacking damage buff while this is active on you.",
			"- Run out of [349663].",
			"- SECOND BOSS: Achillite",
			"- TANK: When the boss is close to dying, move him to a corner to minimize [349987].",
			"- TANK: Use defensive cooldowns during [349934].",
			"- Interrupt [349934] as soon as the shield is down.",
			"- Move away from the group if you're targeted with [349954]. This can be dispelled.",
			"- THIRD BOSS: Venza Goldfuse",
			"- Run out of [350090] and kill the chains from [350101]."
		}
	},
	["Mailroom Mayhem"] =
	{
		["Normal"] = {
			"This fight currently doesn't have a description."
		},
		["Heroic"] = {
			"This fight currently doesn't have a description."
		},
		["Mythic"] = {
			"- Clear all trash before engaging the boss.",
			"- Use healing/defensive cooldowns during [346742].",
			"- Stack together when someone is targeted by [346967].",
			"- Soak the purple circles on the ground during [346286] and dispell the DOT they give if you're able to.",
			"- Pick up the suitcases during [346947] and throw them in open mail chutes."
		}
	},
	["Myza's Oasis"] =
	{
		["Normal"] = {
			"This fight currently doesn't have a description."
		},
		["Heroic"] = {
			"This fight currently doesn't have a description."
		},
		["Mythic"] = {
			"- It's recommended that the TANK takes the microphone, HEALER takes the drums, and DPS take the rest.",
			"- The DPS can move through the buff zones created by the drums/microphone. Once 10 buffs are collected, mobs will spawn.",
			"- On the third set of mobs, the boss (Zo'gron) comes out.",
			"- Spread out for [355438].",
			"- Move behind the boss when he casts [350919]."
		}
	},
	["So'azmi"] =
	{
		["Normal"] = {
			"This fight currently doesn't have a description."
		},
		["Heroic"] = {
			"This fight currently doesn't have a description."
		},
		["Mythic"] = {
			"- The teleporters will teleport you to the matching shape if you move through them.",
			"- Use the teleporters to avoid [347481].",
			"- Interrupt [357188] TWICE! It does massive damage to the group if it gets off."
		}
	},
	["Hylbrande"] =
	{
		["Normal"] = {
			"This fight currently doesn't have a description."
		},
		["Heroic"] = {
			"This fight currently doesn't have a description."
		},
		["Mythic"] = {
			"- TANK: Face the boss away from the group and run out of [347094].",
			"- If targeted by [346959], drop the pools away from the group.",
			"- Priotizie killing adds when they spawn.",
			"- When the 4 colored orbs spawn, they need to be put on the altars in the corners of the room.", 
			"- One player needs to look at the console for the palcement of each orb and communicate it while the other players run the orbs to the correct corners."
		}
	},
	["Timecap'n Hooktail"] =
	{
		["Normal"] = {
			"This fight currently doesn't have a description."
		},
		["Heroic"] = {
			"This fight currently doesn't have a description."
		},
		["Mythic"] = {
			"- Don't stand in front of or behind the boss (unless you're Tank).",
			"- Avoid bad stuff on the ground and don't stand in the water.",
			"- If targeted by the breath attack, use it to hit as many adds as you can."
		}
	},
	["So'leah"] =
	{
		["Normal"] = {
			"This fight currently doesn't have a description."
		},
		["Heroic"] = {
			"This fight currently doesn't have a description."
		},
		["Mythic"] = {
			"- PHASE 1:",
			"- Run into [350799] to drain it, which will give you a stacking DOT. Wait for the DOT to expire before draining another so damage doesn't get out of hand.",
			"- Kill and interrupt the adds.",
			"- PHASE 2 (When the boss hits 40% health):",
			"- Everyone needs to position themselves so that [350885] will hit ALL of the relics when it passes through. Look at the red arrows on the ground to see the direction it will go.",
			"- Dodge the missles that fly out of the relics and the AOE rings around them."
		}
	}
}