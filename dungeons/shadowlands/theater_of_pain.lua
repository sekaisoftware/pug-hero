-- get the namespace
local _, namespace = ...;

namespace.dungeons.shadowlands["Theater of Pain"] = {
	["An Affront of Challengers"] =
	{
		["Normal"] = {
			"- Each of them get a new ability at 40% health, so kill them one at a time to avoid having multiple abilities at once.",
			"- Mass Dispel and enrage dispelling abilities will help a lot during this fight.",
			"- The best order will depend on your groups utility; however, an order that works best for most groups is: Sethel -> Viseren -> Desia",
			"- When [Sethel] gets to 40% health and he'll become unkillable for 60 seconds. You can either wait it out or dispel it if you have that utility in your group.",
			"- When [Viseren] gets to 40%, a disease dot is put on everyone. Dispel it if you have that utility in your group.",
			"- When [Desia] gets to 40%, he enrages and fixates random players. Run away if you get fixated. Remove the enrage if you have that utility in your group.",
		},
		["Heroic"] = {
			"- Each of them get a new ability at 40% health, so kill them one at a time to avoid having multiple abilities at once.",
			"- Mass Dispel and enrage dispelling abilities will help a lot during this fight.",
			"- The best order will depend on your groups utility; however, an order that works best for most groups is: Sethel -> Viseren -> Desia",
			"- When [Sethel] gets to 40% health and he'll become unkillable for 60 seconds. You can either wait it out or dispel it if you have that utility in your group.",
			"- When [Viseren] gets to 40%, a disease dot is put on everyone. Dispel it if you have that utility in your group.",
			"- When [Desia] gets to 40%, he enrages and fixates random players. Run away if you get fixated. Remove the enrage if you have that utility in your group.",
		},
		["Mythic"] = {
			"- Each of them get a new ability at 40% health, so kill them one at a time to avoid having multiple abilities at once.",
			"- Mass Dispel and enrage dispelling abilities will help a lot during this fight.",
			"- The best order will depend on your groups utility; however, an order that works best for most groups is: Sethel -> Viseren -> Desia",
			"- When [Sethel] gets to 40% health and he'll become unkillable for 60 seconds. You can either wait it out or dispel it if you have that utility in your group.",
			"- When [Viseren] gets to 40%, a disease dot is put on everyone. Dispel it if you have that utility in your group.",
			"- When [Desia] gets to 40%, he enrages and fixates random players. Run away if you get fixated. Remove the enrage if you have that utility in your group.",
		}
	},
	["Gorechop"] =
	{
		["Normal"] = {
			"- Hooks will move across the room. Avoid them by moving between the hooks.",
			"- Prioritize killing the adds and spread while they're alive.",
		},
		["Heroic"] = {
			"- Hooks will move across the room. Avoid them by moving between the hooks.",
			"- Prioritize killing the adds and spread while they're alive.",
		},
		["Mythic"] = {
			"- Hooks will move across the room. Avoid them by moving between the hooks.",
			"- Prioritize killing the adds and spread while they're alive.",
		}
	},
	["Xav the Unfallen"] =
	{
		["Normal"] = {
			"- Kill the banner that spawns by the boss.",
			"- Avoid [317231], [320729], and [339415]",
			"- Occasionally two players will be selected to duel in the ring below. The winner gets a damage increase and the loser gets a damage decrease. A player must win within 45 seconds or both players get the damage decrease.",
		},
		["Heroic"] = {
			"- Kill the banner that spawns by the boss.",
			"- Avoid [317231], [320729], and [339415]",
			"- Occasionally two players will be selected to duel in the ring below. The winner gets a damage increase and the loser gets a damage decrease. A player must win within 45 seconds or both players get the damage decrease.",
		},
		["Mythic"] = {
			"- Kill the banner that spawns by the boss.",
			"- Avoid [317231], [320729], and [339415]",
			"- Occasionally two players will be selected to duel in the ring below. The winner gets a damage increase and the loser gets a damage decrease. A player must win within 45 seconds or both players get the damage decrease.",
		}
	},
	["Kul'tharok"] =
	{
		["Normal"] = {
			"- Tank: Always be within melee range of the boss",
			"- Stay slightly spread out throughout the fight.",
			"- Dodge the green swirls and avoid the green circles on the ground.",
			"- If targeted by [319521], move behind one of the green circles on the ground so the circle is between you and the boss. Collect your soul to be able to use abilities again.",
		},
		["Heroic"] = {
			"- Tank: Always be within melee range of the boss",
			"- Stay slightly spread out throughout the fight.",
			"- Dodge the green swirls and avoid the green circles on the ground.",
			"- If targeted by [319521], move behind one of the green circles on the ground so the circle is between you and the boss. Collect your soul to be able to use abilities again.",
		},
		["Mythic"] = {
			"- Tank: Always be within melee range of the boss",
			"- Stay slightly spread out throughout the fight.",
			"- Dodge the green swirls and avoid the green circles on the ground.",
			"- If targeted by [319521], move behind one of the green circles on the ground so the circle is between you and the boss. Collect your soul to be able to use abilities again.",
		}
    },
    ["Modretha, the Endless Empress"] =
	{
		["Normal"] = {
			"- Avoid the frontal beam.",
			"- Everyone will get targeted with green circles. Spread out just enough so that your circle won't drop on another player, but keep them as close as possible.",
			"- Interrupt, CC, and kill the adds that spawn from the circles quickly.",
			"- Don't get pulled into the green riff.",
		},
		["Heroic"] = {
			"- Avoid the frontal beam.",
			"- Everyone will get targeted with green circles. Spread out just enough so that your circle won't drop on another player, but keep them as close as possible.",
			"- Interrupt, CC, and kill the adds that spawn from the circles quickly.",
			"- Don't get pulled into the green riff.",
		},
		["Mythic"] = {
			"- Avoid the frontal beam.",
			"- Everyone will get targeted with green circles. Spread out just enough so that your circle won't drop on another player, but keep them as close as possible.",
			"- Interrupt, CC, and kill the adds that spawn from the circles quickly.",
			"- Don't get pulled into the green riff.",
		}
	}
}