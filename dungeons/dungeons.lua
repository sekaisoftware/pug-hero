-- get the namespace
local _, namespace = ...;

-- dungeon names must match the instance name in WoW
namespace.dungeons = {}
namespace.dungeons.bfa = {}
namespace.dungeons.shadowlands = {}

-- id mapping for quick localization fix
-- bfa
namespace.mappings[1763] = "Atal'Dazar"
namespace.mappings[1754] = "Freehold"
namespace.mappings[1762] = "Kings' Rest"
namespace.mappings[1864] = "Shrine of the Storm"
namespace.mappings[1822] = "Siege of Boralus"
namespace.mappings[1877] = "Temple of Sethraliss"
namespace.mappings[1594] = "The MOTHERLODE!!"
namespace.mappings[1841] = "The Underrot"
namespace.mappings[1771] = "Tol Dagor"
namespace.mappings[1862] = "Waycrest Manor"
namespace.mappings[2097] = "Operation: Mechagon"
-- shadowlands
namespace.mappings[2291] = "De Other Side"
namespace.mappings[2287] = "Halls of Atonement"
namespace.mappings[2290] = "Mists of Tirna Scithe"
namespace.mappings[2286] = "The Necrotic Wake"
namespace.mappings[2289] = "Plaguefall"
namespace.mappings[2284] = "Sanguine Depths"
namespace.mappings[2285] = "Spires of Ascension"
namespace.mappings[2293] = "Theater of Pain"
namespace.mappings[2441] = "Tazavesh the Veiled Market"

-- spell to id mappings
-- siege of boralus
namespace.mappings["On the Hook"] = 257459
namespace.mappings["Tidal Surge"] = 276068
namespace.mappings["Putrid Waters"] = 274991
-- atal'dazar
namespace.mappings["Soulrend"] = 249924
namespace.mappings["Soulspawn"] = 249926
namespace.mappings["Toxic Pools"] = 250585
namespace.mappings["Noxious Stench"] = 250368
namespace.mappings["Terrifying Visage"] = 255371
namespace.mappings["Pursue"] = 257407
namespace.mappings["Spirit of Gold"] = 259205
namespace.mappings["Transfusion"] = 255577
-- tol dagor
namespace.mappings["Flashing Daggers"] = 257785
namespace.mappings["Howling Fear"] = 257791
namespace.mappings["Ignition"] = 256976
namespace.mappings["Cinderflame"] = 256955
namespace.mappings["Fuselighter"] = 257033
namespace.mappings["Deadeye"] = 256044
namespace.mappings["Azerite Rounds: Blast"] = 256199
namespace.mappings["Sand Traps"] = 257119
namespace.mappings["Upheaval"] = 257617
-- waycrest manor
namespace.mappings["Focusing Iris"] = 260805
namespace.mappings["Soul Manipulation"] = 260907
namespace.mappings["Unstable Runic Mark"] = 260702
namespace.mappings["Rotten Expulsion"] = 264694
namespace.mappings["Call Servant"] = 264931
namespace.mappings["Soul Thorns"] = 260551
namespace.mappings["Wildfire"] = 260570
namespace.mappings["Virulent Pathogen"] = 261439
namespace.mappings["Discordant Cadenza"] = 268306
namespace.mappings["Alchemical Fire"] = 266198
-- kings rest
namespace.mappings["Spit Gold"] = 265773
namespace.mappings["Barrel Through"] = 266951
namespace.mappings["Debilitating Backhand"] = 266237
namespace.mappings["Shattered Defenses"] = 266238
namespace.mappings["Poison Nova"] = 267273
namespace.mappings["Drain Fluids"] = 267618
namespace.mappings["Quaking Leap"] = 268932
-- temple of sethraliss
namespace.mappings["Consume Charge"] = 266512
namespace.mappings["Galvonized"] = 265973
namespace.mappings["Lightning Shield"] = 263246
namespace.mappings["Arc Dash"] = 263425
namespace.mappings["A Knot of Snakes"] = 263958
namespace.mappings["Blinding Sand"] = 263914
namespace.mappings["Burrow"] = 264206
-- underrot
namespace.mappings["Blood Mirror"] = 264603
namespace.mappings["Creeping Rot"] = 260894
-- two upheavals. use id for this one.
--namespace.mappings["Upheaval"] = 259718
namespace.mappings["Shockwave"] = 272457
namespace.mappings["Festering Harvest"] = 259732
namespace.mappings["Blood Visages"] = 1
namespace.mappings["Cleansing Light"] = 269310
namespace.mappings["Putrid Blood"] = 269301
-- motherlode
namespace.mappings["Homing Missle"] = 260838
namespace.mappings["Propellant Blast"] = 259940
namespace.mappings["Shocking Claw"] = 257337
namespace.mappings["Resonant Quake"] = 258628
-- shrine of the storm
namespace.mappings["Yawning Gate"] = 269399
namespace.mappings["Whispers of Power"] = 267034
namespace.mappings["Slicing Blast"] = 267818
namespace.mappings["Reinforcing Ward"] = 267905
namespace.mappings["Swiftness Ward"] = 267891
namespace.mappings["Hindering Cleave"] = 267899
namespace.mappings["Blessing of Ironsides"] = 267901
namespace.mappings["Surging Rush"] = 264155
namespace.mappings["Undertow"] = 264166
namespace.mappings["Choking Brine"] = 264560
namespace.mappings["Void Bolt"] = 268347
namespace.mappings["Mind Rend"] = 268896
namespace.mappings["Ancient Mindbender"] = 269131
-- freehold
namespace.mappings["Blackout Barrel"] = 258337
namespace.mappings["Revitalizing Brew"] = 256060
namespace.mappings["Azerite Powder Shot"] = 256106
-- mechagon
namespace.mappings["Wreck"] = 295445
namespace.mappings["Mega Taze"] = 298718
namespace.mappings["Venting Flames"] = 291946
namespace.mappings["Blazing Chomp"] = 294929
namespace.mappings["Giga Zap"] = 291928
namespace.mappings["Cutting Beam"] = 296331
namespace.mappings["Charged Smash"] = 297254
