# PUG Hero

Are you tired of wiping in dungeons and raids because your group doesn't understand the fight?

PUG Hero aims to solve this problem by providing clear, concise information about a fight and allowing the player to put it in chat for the group to see. Using PUG Hero, any group can immediately understand those critical mechanics that would otherwise cause a wipe.

## Quick Overview

PUG Hero is an icon that sits on your minimap which you can click to explain or see fights for a given instance which you are in.

Key features include:

- Select the chat channel to display the description to just yourself, your guild, or the entire group. 
- Edit any fight description in the PUG Hero options menu to match your preferred strategy.
- Fight descriptions for all Battle for Azeroth dungeons on Normal, Heroic, and Mythic difficulties.
- Fight descriptions for Ny'alotha, the Waking City on LFR, Normal, Heroic, and most Mythic difficulties.
- Fight descriptions for all Shadowlands dungeons on Normal, Heroic, and Mythic difficulties.
- Fight descriptions for Castle Nathria on LFR, Normal, and Heroic difficulties.

## Download

Download PUG Hero here: https://www.curseforge.com/wow/addons/pug-hero
 

 

 

 