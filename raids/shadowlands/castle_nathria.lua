-- get the namespace
local _, namespace = ...;

namespace.raids.shadowlands["Castle Nathria"] = {
	["Shriekwing"] =
	{
		["Looking For Raid"] = {
            "- PHASE 1:",
            "- Tanks: Taunt after each [328857] cast.",
            "- Everyone should hide behind the same pillar when [330711] is cast.",
            "- Move to the edge of the room when marked with [342074].",
            "- PHASE 2:",
            "- Stay away from the boss at all times.",
            "- Dodge the sonic rings bouncing around the room.",
            "- Hide behind the pillars to line of sight [330711]."
		},
		["Normal"] = {
			"- PHASE 1:",
            "- Tanks: Taunt after each [328857] cast.",
            "- Everyone should hide behind the same pillar when [330711] is cast.",
            "- Move to the edge of the room when marked with [342074].",
            "- PHASE 2:",
            "- Stay away from the boss at all times.",
            "- Dodge the sonic rings bouncing around the room.",
            "- Hide behind the pillars to line of sight [330711]."
		},
		["Heroic"] = {
			"- PHASE 1:",
            "- Tanks: Taunt after each [328857] cast.",
            "- Everyone should hide behind the same pillar when [330711] is cast.",
            "- Move to the edge of the room when marked with [342074].",
            "- Heroic Specific: Bait the [342863] towards an open area and then dodge the sonic rings the boss shoots out.",
            "- PHASE 2:",
            "- Stay away from the boss at all times.",
            "- Dodge the sonic rings bouncing around the room.",
            "- Hide behind the pillars to line of sight [330711]."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Huntsman Altimor"] =
	{
		["Looking For Raid"] = {
            "- Spread loosely around the boss to reduce damage of [334404].",
            "- Make sure only the marked player is in the path when [335114] is cast.",
            "- The boss and his active pet share a health pool. Each pet has different mechanics to pay attention to.",
            "- Margore - Tanks should taunt at around 2-3 stacks of [334971]. Everyone should stack on the player marked for the [334939].",
            "- Bargast - Healers need to heal up the [334797] add before it reaches the boss. Move the boss away from and CC adds spawned by [334757]. Kill the adds off and interrupt their [334708].",
            "- Hecutis - Tanks should move him to drop his [334860] stacks, but do so gradually as it will damage the raid. Players should move away from the raid when afflicted with [334852] and drop the pool away from the group.",
		},
		["Normal"] = {
			"- Spread loosely around the boss to reduce damage of [334404].",
            "- Make sure only the marked player is in the path when [335114] is cast.",
            "- The boss and his active pet share a health pool. Each pet has different mechanics to pay attention to.",
            "- Margore - Tanks should taunt at around 2-3 stacks of [334971]. Everyone should stack on the player marked for the [334939].",
            "- Bargast - Healers need to heal up the [334797] add before it reaches the boss. Move the boss away from and CC adds spawned by [334757]. Kill the adds off and interrupt their [334708].",
            "- Hecutis - Tanks should move him to drop his [334860] stacks, but do so gradually as it will damage the raid. Players should move away from the raid when afflicted with [334852] and drop the pool away from the group.",
		},
		["Heroic"] = {
			"- Spread loosely around the boss to reduce damage of [334404].",
            "- Make sure only the marked player is in the path when [335114] is cast.",
            "- The boss and his active pet share a health pool. Each pet has different mechanics to pay attention to.",
            "- Margore - Tanks should taunt at around 2-3 stacks of [334971]. Everyone should stack on the player marked for the [334939].",
            "- Bargast - Healers need to heal up the [334797] add before it reaches the boss. Move the boss away from and CC adds spawned by [334757]. Kill the adds off and interrupt their [334708].",
            "- Hecutis - Tanks should move him to drop his [334860] stacks, but do so gradually as it will damage the raid. Players should move away from the raid when afflicted with [334852] and drop the pool away from the group.",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Hungering Destroyer"] =
	{
		["Looking For Raid"] = {
            "- Tanks: taunt after each [329774].",
            "- Move the boss to teh edge of the room and run away from him when he casts [334522].",
            "- Spread out during [329742].",
            "- At least 2-3 players should help soak players afflicted with [329298].",
            "- Players marked with [334266] lines should spread to avoid hitting other players with their line.",
		},
		["Normal"] = {
			"- Tanks: taunt after each [329774].",
            "- Move the boss to teh edge of the room and run away from him when he casts [334522].",
            "- Spread out during [329742].",
            "- At least 2-3 players should help soak players afflicted with [329298].",
            "- Players marked with [334266] lines should spread to avoid hitting other players with their line.",
		},
		["Heroic"] = {
			"- Tanks: taunt after each [329774].",
            "- Move the boss to teh edge of the room and run away from him when he casts [334522].",
            "- Spread out during [329742].",
            "- At least 2-3 players should help soak players afflicted with [329298].",
            "- Players marked with [334266] lines should spread to avoid hitting other players with their line.",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Lady Inerva Darkvein"] =
	{
		["Looking For Raid"] = {
            "- Drain the Anima Containers by clicking on them before and after the boss casts [331844] or if they get above 33%+ energy.",
            "- Each anima container has different mechanics when they're emptied as explained below:",
            "- [325379] - Tanks should taunt at two stacks of [325382]. At 33%, Healers should focus healing on those afflicted with [325908]. At 66%, tanks also need to move away from the raid when [325382] is about to expire.",
            "- [325769] - Everyone should soak the vial landing locations. At 33%, the vials leave behind pools you should get out of. At 66%, the vials bonce and need to be soaked a second time.",
            "- [342288] - [324983] players need to cut through each of hte three orbs with their beams at the same time. At 33% and 66%, everyone should avoid the extra beams.",
            "- [342321] - Move the debuff near the boss and kill the add that spawns from it. At 33%, the debuffed player is rooted so bring the boss to them instead. At 66%, also dodge the orbs.",
		},
		["Normal"] = {
			"- Drain the Anima Containers by clicking on them before and after the boss casts [331844] or if they get above 33%+ energy.",
            "- Each anima container has different mechanics when they're emptied as explained below:",
            "- [325379] - Tanks should taunt at two stacks of [325382]. At 33%, Healers should focus healing on those afflicted with [325908]. At 66%, tanks also need to move away from the raid when [325382] is about to expire.",
            "- [325769] - Everyone should soak the vial landing locations. At 33%, the vials leave behind pools you should get out of. At 66%, the vials bonce and need to be soaked a second time.",
            "- [342288] - [324983] players need to cut through each of hte three orbs with their beams at the same time. At 33% and 66%, everyone should avoid the extra beams.",
            "- [342321] - Move the debuff near the boss and kill the add that spawns from it. At 33%, the debuffed player is rooted so bring the boss to them instead. At 66%, also dodge the orbs.",
		},
		["Heroic"] = {
			"- Drain the Anima Containers by clicking on them before and after the boss casts [331844] or if they get above 33%+ energy.",
            "- Each anima container has different mechanics when they're emptied as explained below:",
            "- [325379] - Tanks should taunt at two stacks of [325382]. At 33%, Healers should focus healing on those afflicted with [325908]. At 66%, tanks also need to move away from the raid when [325382] is about to expire.",
            "- [325769] - Everyone should soak the vial landing locations. At 33%, the vials leave behind pools you should get out of. At 66%, the vials bonce and need to be soaked a second time.",
            "- [342288] - [324983] players need to cut through each of hte three orbs with their beams at the same time. At 33% and 66%, everyone should avoid the extra beams.",
            "- [342321] - Move the debuff near the boss and kill the add that spawns from it. At 33%, the debuffed player is rooted so bring the boss to them instead. At 66%, also dodge the orbs.",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Artificer Xy'Mox"] =
	{
		["Looking For Raid"] = {
            "- You can instantly teleport between the two wormhole locations by moving into one of them.",
            "- Tanks: Take turns taking the [325361] and run far away when it's applied to you.",
            "- Players marked with [328437] need to place the wormhole in specific locations depending on what relic is active.",
            "- Relic 1 (70%-100% boss health): Place [328437] on opposite sides of the room. The raid should be positioned around one of these wormholes. Kite the [Fleeting Spirit] add if you're fixated and damage players who become mind controlled to free them.",
            "- Relic 2 (40%-70% boss health): Place [328437] on opposite sides of the room. The raid should be positioned around one of these wormholes. Players should pick up and move [329090] far away from the raid.",
            "- Relic 3 (0%-40% boss health): Place [328437] in the middle of the room and at one of the edges of the room. Run into the wormhole in the middle of the room to avoid [328880]. Try to keep stacked throughout this phase so healers can keep up with the constant damage.",
		},
		["Normal"] = {
            "- You can instantly teleport between the two wormhole locations by moving into one of them.",
            "- Tanks: Take turns taking the [325361] and run far away when it's applied to you.",
            "- Players marked with [328437] need to place the wormhole in specific locations depending on what relic is active.",
            "- Relic 1 (70%-100% boss health): Place [328437] on opposite sides of the room. The raid should be positioned around one of these wormholes. Kite the [Fleeting Spirit] add if you're fixated and damage players who become mind controlled to free them.",
            "- Relic 2 (40%-70% boss health): Place [328437] on opposite sides of the room. The raid should be positioned around one of these wormholes. Players should pick up and move [329090] far away from the raid.",
            "- Relic 3 (0%-40% boss health): Place [328437] in the middle of the room and at one of the edges of the room. Run into the wormhole in the middle of the room to avoid [328880]. Try to keep stacked throughout this phase so healers can keep up with the constant damage.",
		},
		["Heroic"] = {
            "- You can instantly teleport between the two wormhole locations by moving into one of them.",
            "- Tanks: Take turns taking the [325361] and run far away when it's applied to you.",
            "- Players marked with [328437] need to place the wormhole in specific locations depending on what relic is active.",
            "- Relic 1 (70%-100% boss health): Place [328437] on opposite sides of the room. The raid should be positioned around one of these wormholes. Kite the [Fleeting Spirit] add if you're fixated and damage players who become mind controlled to free them.",
            "- Relic 2 (40%-70% boss health): Place [328437] on opposite sides of the room. The raid should be positioned around one of these wormholes. Players should pick up and move [329090] far away from the raid.",
            "- Relic 3 (0%-40% boss health): Place [328437] in the middle of the room and at one of the edges of the room. Run into the wormhole in the middle of the room to avoid [328880]. Try to keep stacked throughout this phase so healers can keep up with the constant damage.",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Sun King's Salvation"] =
	{
		["Looking For Raid"] = {
            "- PHASE 1:",
            "- Heal Kael'thas Sunstrider. Players can also sacrifice their own health using Soul Pedestals every 2 minutes.",
            "- During this phase you'll have to deal with numerous adds:",
            "- High Torturer Darithos - Kill him first, he's the only add that doesn't respawn.",
            "- Rockbound Vanquisher - Kill them before [325506] is cast too many times. Tanks should taunt if [325442] damage get's too high.",
            "- Bleakwing Assassins - Spread out when marked with [341473]. Burst them down before they finish casting [333145], which they start casting at 30% health.'",
            "- Vile Occultist - Interrupt [333002] or dispel the debuff it applies. Heal the [Essence Font] they drop for a burst of healing on [Kael'thas Sunstrider].",
            "- Pestering Fiend - Cleave them down.",
            "- Soul Infuser - CC and kill them before they reach [Kael'thas Sunstrider]. When they die, a healer should collect the orb to get the buff [326078].",
            "- PHASE 2 (45% and 90% health):",
            "- [Kael'thas Sunstrider] cannot be healed during this phase directly, but damaging the shades will heal him.",
            "- Bring the [Shade of Kael'thas] add to the side of the room away from the [Reborn Phoenix] adds. CC and kill the Phoenix adds.",
            "- Tanks: Face the [Shade of Kael'thas] add away from the group and taunt off of each other when [326456] stacks expire.",
            "- If targeted with [325877], move out of the group. Other players should go with to soak the target marked by [325877].",
		},
		["Normal"] = {
			"- PHASE 1:",
            "- Heal Kael'thas Sunstrider. Players can also sacrifice their own health using Soul Pedestals every 2 minutes.",
            "- During this phase you'll have to deal with numerous adds:",
            "- High Torturer Darithos - Kill him first, he's the only add that doesn't respawn.",
            "- Rockbound Vanquisher - Kill them before [325506] is cast too many times. Tanks should taunt if [325442] damage get's too high.",
            "- Bleakwing Assassins - Spread out when marked with [341473]. Burst them down before they finish casting [333145], which they start casting at 30% health.'",
            "- Vile Occultist - Interrupt [333002] or dispel the debuff it applies. Heal the [Essence Font] they drop for a burst of healing on [Kael'thas Sunstrider].",
            "- Pestering Fiend - Cleave them down.",
            "- Soul Infuser - CC and kill them before they reach [Kael'thas Sunstrider]. When they die, a healer should collect the orb to get the buff [326078].",
            "- PHASE 2 (45% and 90% health):",
            "- [Kael'thas Sunstrider] cannot be healed during this phase directly, but damaging the shades will heal him.",
            "- Bring the [Shade of Kael'thas] add to the side of the room away from the [Reborn Phoenix] adds. CC and kill the Phoenix adds.",
            "- Tanks: Face the [Shade of Kael'thas] add away from the group and taunt off of each other when [326456] stacks expire.",
            "- If targeted with [325877], move out of the group. Other players should go with to soak the target marked by [325877].",
		},
		["Heroic"] = {
			"- PHASE 1:",
            "- Heal Kael'thas Sunstrider. Players can also sacrifice their own health using Soul Pedestals every 2 minutes.",
            "- During this phase you'll have to deal with numerous adds:",
            "- High Torturer Darithos - Kill him first, he's the only add that doesn't respawn.",
            "- Rockbound Vanquisher - Kill them before [325506] is cast too many times. Tanks should taunt if [325442] damage get's too high.",
            "- Bleakwing Assassins - Spread out when marked with [341473]. Burst them down before they finish casting [333145], which they start casting at 30% health.'",
            "- Vile Occultist - Interrupt [333002] or dispel the debuff it applies. Heal the [Essence Font] they drop for a burst of healing on [Kael'thas Sunstrider].",
            "- Pestering Fiend - Cleave them down. (Heroic Specific) Stay away from them when they die because they will explode.",
            "- Soul Infuser - CC and kill them before they reach [Kael'thas Sunstrider]. When they die, a healer should collect the orb to get the buff [326078].",
            "- PHASE 2 (45% and 90% health):",
            "- [Kael'thas Sunstrider] cannot be healed during this phase directly, but damaging the shades will heal him.",
            "- Bring the [Shade of Kael'thas] add to the side of the room away from the [Reborn Phoenix] adds. CC and kill the Phoenix adds.",
            "- Tanks: Face the [Shade of Kael'thas] add away from the group and taunt off of each other when [326456] stacks expire.",
            "- If targeted with [325877], move out of the group. Other players should go with to soak the target marked by [325877].",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Council of Blood"] =
	{
		["Looking For Raid"] = {
            "- Focus killing one boss at a time in this order: [Baroness Frieda] > [Castellan Niklaus] > [Lord Stavros].",
            "- When a boss reaches 50%, get to your dance position and use the buttons as instructed.",
            "- Each boss has different mechanics according to which bosses have been killed. This explanation assumes you're killing them in the order mentioned above.",
            "- Baroness Frieda - Interrupt [337110] or dispel the debuffs if a cast gets out.",
            "- Castellan Niklaus - Tanks should swap after two [346690] casts. Nuke down the [Dutiful Attendant] add which makes the lowest health boss immune. When 1 boss is dead, also CC and kill the [Dredger Servant] add.",
            "- Lord Stavros - Tanks should face this boss away from other players and taunt after 2 casts of [327503]. When paired with [331634], find your partner and stay near them. When 1 boss is dead, also move away from the [327619] dancers.",
		},
		["Normal"] = {
			"- Focus killing one boss at a time in this order: [Baroness Frieda] > [Castellan Niklaus] > [Lord Stavros].",
            "- When a boss reaches 50%, get to your dance position and use the buttons as instructed.",
            "- Each boss has different mechanics according to which bosses have been killed. This explanation assumes you're killing them in the order mentioned above.",
            "- Baroness Frieda - Interrupt [337110] or dispel the debuffs if a cast gets out.",
            "- Castellan Niklaus - Tanks should swap after two [346690] casts. Nuke down the [Dutiful Attendant] add which makes the lowest health boss immune. When 1 boss is dead, also CC and kill the [Dredger Servant] add.",
            "- Lord Stavros - Tanks should face this boss away from other players and taunt after 2 casts of [327503]. When paired with [331634], find your partner and stay near them. When 1 boss is dead, also move away from the [327619] dancers.",
		},
		["Heroic"] = {
			"- Focus killing one boss at a time in this order: [Baroness Frieda] > [Castellan Niklaus] > [Lord Stavros].",
            "- When a boss reaches 50%, get to your dance position and use the buttons as instructed.",
            "- Each boss has different mechanics according to which bosses have been killed. This explanation assumes you're killing them in the order mentioned above.",
            "- Baroness Frieda - Interrupt [337110] or dispel the debuffs if a cast gets out.",
            "- Castellan Niklaus - Tanks should swap after two [346690] casts. Nuke down the [Dutiful Attendant] add which makes the lowest health boss immune. When 1 boss is dead, also CC and kill the [Dredger Servant] add.",
            "- Lord Stavros - Tanks should face this boss away from other players and taunt after 2 casts of [327503]. When paired with [331634], find your partner and stay near them. When 1 boss is dead, also move away from the [327619] dancers.",
            "- Lord Stavros (Heroic Specific) - When 2 bosses are dead, find the single [Dancing Fools], kill them, and stand near their corpses for a safe spot.",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Sludgefist"] =
	{
		["Looking For Raid"] = {
            "- Tanks should stack on each other to share the damage of [335297].",
            "- Tanks should keep the boss away from pillars during the fight so the boss doesn't break them.",
            "- Ranged players should keep far away from the boss to bait [332572] into lower traffic areas.",
            "- When afflicted with [335300] stay near your partner.",
            "- When marked by [331209], hide behind a pillar so the boss runs into it. Pop DPS cooldowns after the boss hits the pillar.",
		},
		["Normal"] = {
			"- Tanks should stack on each other to share the damage of [335297].",
            "- Tanks should keep the boss away from pillars during the fight so the boss doesn't break them.",
            "- Ranged players should keep far away from the boss to bait [332572] into lower traffic areas.",
            "- When afflicted with [335300] stay near your partner.",
            "- When marked by [331209], hide behind a pillar so the boss runs into it. Pop DPS cooldowns after the boss hits the pillar.",
		},
		["Heroic"] = {
			"- Tanks should stack on each other to share the damage of [335297].",
            "- Tanks should keep the boss away from pillars during the fight so the boss doesn't break them.",
            "- Ranged players should keep far away from the boss to bait [332572] into lower traffic areas.",
            "- When afflicted with [335300] stay near your partner.",
            "- When marked by [331209], hide behind a pillar so the boss runs into it. Pop DPS cooldowns after the boss hits the pillar.",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Stone Legion Generals"] =
	{
		["Looking For Raid"] = {
            "- ALL PHASES:",
            "- Players marked by [333387] should move to the side of the boss and spread. Other players should avoid them as to not be hit by the blade.",
            "- Kill adds and deliver the [332393] they drop to [Prince Renathal] (by the door). Once he's at full mana, move near him to avoid being knocked off.",
            "- PHASE 1 - Fighting Kaal:",
            "- Drop [343881] stacks by standing in the [339693].",
            "- Healers should dispel [334765].",
            "- PHASE 2 - Fighting Grashaal:",
            "- Tanks should taunt to allow for [342425] stacks to drop.",
            "- Move away when targeted by [344500] and soak [344503].",
            "- PHASE 3 - Fighting both at once:",
            "- Dodge the ground mechanics and kill them as close together as you can.",
		},
		["Normal"] = {
			"- ALL PHASES:",
            "- Players marked by [333387] should move to the side of the boss and spread. Other players should avoid them as to not be hit by the blade.",
            "- Kill adds and deliver the [332393] they drop to [Prince Renathal] (by the door). Once he's at full mana, move near him to avoid being knocked off.",
            "- PHASE 1 - Fighting Kaal:",
            "- Drop [343881] stacks by standing in the [339693].",
            "- Healers should dispel [334765].",
            "- PHASE 2 - Fighting Grashaal:",
            "- Tanks should taunt to allow for [342425] stacks to drop.",
            "- Move away when targeted by [344500] and soak [344503].",
            "- PHASE 3 - Fighting both at once:",
            "- Dodge the ground mechanics and kill them as close together as you can.",
		},
		["Heroic"] = {
			"- ALL PHASES:",
            "- Players marked by [333387] should move to the side of the boss and spread. Other players should avoid them as to not be hit by the blade.",
            "- Kill adds and deliver the [332393] they drop to [Prince Renathal] (by the door). Once he's at full mana, move near him to avoid being knocked off.",
            "- PHASE 1 - Fighting Kaal:",
            "- Drop [343881] stacks by standing in the [339693].",
            "- Healers should dispel [334765].",
            "- PHASE 2 - Fighting Grashaal:",
            "- Tanks should taunt to allow for [342425] stacks to drop.",
            "- Move away when targeted by [344500] and soak [344503].",
            "- PHASE 3 - Fighting both at once:",
            "- Dodge the ground mechanics and kill them as close together as you can.",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Sire Denathrius"] =
	{
		["Looking For Raid"] = {
            "- PHASE 1 (70%-100% health):",
            "- When targeted by [327089], move away from others.",
            "- Split the group into two halves and rotate out standing in the [326707] cast. Taking damage from this cast spawns an add and removes a stack of [326699].",
            "- Make sure you have less than 3 stacks of [326699] before the next phase.",
            "- Prioritize killing the [Echo of Sin] adds.",
            "- At the end of the phase, run to the middle of the room to survive [328276].",
            "- PHASE 2 (40%-70% health):",
            "- Prioritize killing the [Crimson Cabalist] adds.",
            "- Tanks should taunt after each [329181] cast and position it to hit the adds and not the raid.",
            "- Tanks should also position the boss near a mirror for the [330627] and all players should run through the mirror to avoid the attack.",
            "- Move away from players marked for [329974].",
            "- PHASE 3 (0%-40% Health):",
            "- Finish off any remaining [Crimson Cabalist] adds.",
            "- Stand around the middle to avoid the [326005] goo.",
            "- Tanks should position the boss towards the edges so that players are knocked towards the middle for [332619].",
            "- Move away from players afflicted with [332797].",
		},
		["Normal"] = {
			"- PHASE 1 (70%-100% health):",
            "- When targeted by [327089], move away from others.",
            "- Split the group into two halves and rotate out standing in the [326707] cast. Taking damage from this cast spawns an add and removes a stack of [326699].",
            "- Make sure you have less than 3 stacks of [326699] before the next phase.",
            "- Prioritize killing the [Echo of Sin] adds.",
            "- At the end of the phase, run to the middle of the room to survive [328276].",
            "- PHASE 2 (40%-70% health):",
            "- Prioritize killing the [Crimson Cabalist] adds.",
            "- Tanks should taunt after each [329181] cast and position it to hit the adds and not the raid.",
            "- Tanks should also position the boss near a mirror for the [330627] and all players should run through the mirror to avoid the attack.",
            "- Move away from players marked for [329974].",
            "- PHASE 3 (0%-40% Health):",
            "- Finish off any remaining [Crimson Cabalist] adds.",
            "- Stand around the middle to avoid the [326005] goo.",
            "- Tanks should position the boss towards the edges so that players are knocked towards the middle for [332619].",
            "- Move away from players afflicted with [332797].",
		},
		["Heroic"] = {
			"- PHASE 1 (70%-100% health):",
            "- When targeted by [327089], move away from others.",
            "- Split the group into two halves and rotate out standing in the [326707] cast. Taking damage from this cast spawns an add and removes a stack of [326699].",
            "- Make sure you have less than 3 stacks of [326699] before the next phase.",
            "- Prioritize killing the [Echo of Sin] adds.",
            "- At the end of the phase, run to the middle of the room to survive [328276].",
            "- PHASE 2 (40%-70% health):",
            "- Prioritize killing the [Crimson Cabalist] adds.",
            "- Tanks should taunt after each [329181] cast and position it to hit the adds and not the raid.",
            "- Tanks should also position the boss near a mirror for the [330627] and all players should run through the mirror to avoid the attack.",
            "- Move away from players marked for [329974].",
            "- PHASE 3 (0%-40% Health):",
            "- Finish off any remaining [Crimson Cabalist] adds.",
            "- Stand around the middle to avoid the [326005] goo.",
            "- Tanks should position the boss towards the edges so that players are knocked towards the middle for [332619].",
            "- Move away from players afflicted with [332797].",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
}