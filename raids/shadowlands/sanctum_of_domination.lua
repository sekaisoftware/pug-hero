-- get the namespace
local _, namespace = ...;

namespace.raids.shadowlands["Sanctum of Domination"] = {
	["The Tarragrue"] =
	{
		["Looking For Raid"] = {
			"- For the anima powers you gain beforehand when killing the mobs, do NOT take [337938] or [347988]. AT LEAST ONE person should take [338733].",
			"- TANKS: Swap on every cast of [346985].",
			"- HEALERS: Dispel players that are feared.",
			"- If you get the debuff [347283], get away from others in the group until it expires.",
			"- Anyone with immunity to stuns needs to intercept [347269] when it targets a player. [338733] can help with this if it was chosen.",
			"- Move to each safe spot during [347671].",
			"- A druid/hunter/rogue should remove the enrage [347490] from the boss.",
            "- Lust/Hero right at the start.",
		},
		["Normal"] = {
			"- For the anima powers you gain beforehand when killing the mobs, do NOT take [337938] or [347988]. AT LEAST ONE person should take [338733].",
			"- TANKS: Swap on every cast of [346985].",
			"- HEALERS: Dispel players that are feared.",
			"- If you get the debuff [347283], get away from others in the group until it expires.",
			"- Anyone with immunity to stuns needs to intercept [347269] when it targets a player. [338733] can help with this if it was chosen.",
			"- Move to each safe spot during [347671].",
			"- A druid/hunter/rogue should remove the enrage [347490] from the boss.",
			"- Lust/Hero right at the start.",
		},
		["Heroic"] = {
			"- For the anima powers you gain beforehand when killing the mobs, do NOT take [337938] or [347988]. AT LEAST ONE person should take [338733].",
			"- TANKS: Swap on every cast of [346985].",
			"- HEALERS: Dispel players that are feared.",
			"- If you get the debuff [347283], get away from others in the group until it expires.",
			"- Anyone with immunity to stuns needs to intercept [347269] when it targets a player. [338733] can help with this if it was chosen.",
			"- Move to each safe spot during [347671].",
			"- A druid/hunter/rogue should remove the enrage [347490] from the boss.",
			"- Soak the circles on the ground. Everyone except tanks should soak the Gray ones. Only tanks should soak the purple ones. Anyone can soak the red ones.",
			"- Lust/Hero right at the start."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["The Eye of the Jailer"] =
	{
		["Looking For Raid"] = {
            "- PHASE 1:",
			"- TANKS: Use defensive cooldowns to mitigate damage from [350828].",
			"- Use grappling hooks (or immunities/teleports) to avoid [350763] (giant laser).",
			"- Get out of the swirls on the ground. If you get hit by one, you'll need to run over your soul fragments to remove a debuff.",
			"- PHASE 2: (At 75%, 50%, and 25% health)",
			"- Keep the adds 30+ yards away from each other to avoid [351816].",
			"- Both adds need to die around the same time to avoid [351994].",
			"- Stay away from players with a circle around them ([350847]).",
			"- Stay away from the front of the platform to keep it clear of [350808].",
			"- Lust/Hero once the boss returns back to PHASE 1 at 25% health."
		},
		["Normal"] = {
            "- PHASE 1:",
			"- TANKS: Use defensive cooldowns to mitigate damage from [350828].",
			"- Use grappling hooks (or immunities/teleports) to avoid [350763] (giant laser).",
			"- Get out of the swirls on the ground. If you get hit by one, you'll need to run over your soul fragments to remove a debuff.",
			"- PHASE 2: (At 75%, 50%, and 25% health)",
			"- Keep the adds 30+ yards away from each other to avoid [351816].",
			"- Both adds need to die around the same time to avoid [351994].",
			"- Stay away from players with a circle around them ([350847]).",
			"- Stay away from the front of the platform to keep it clear of [350808].",
			"- Lust/Hero once the boss returns back to PHASE 1 at 25% health."
		},
		["Heroic"] = {
            "- PHASE 1:",
			"- TANKS: Use defensive cooldowns to mitigate damage from [350828].",
			"- Use grappling hooks (or immunities/teleports) to avoid [350763] (giant laser).",
			"- Get out of the swirls on the ground. If you get hit by one, you'll need to run over your soul fragments to remove a debuff.",
			"- Stack on the player targeted by [349979] (the circle), and then run away from the edge to pull the add down.",
			"- PHASE 2: (At 75%, 50%, and 25% health)",
			"- Keep the adds 30+ yards away from each other to avoid [351816].",
			"- Both adds need to die around the same time to avoid [351994].",
			"- Stay away from players with a circle around them ([350847]).",
			"- Stay away from the front of the platform to keep it clear of [350808].",
			"- Lust/Hero once the boss returns back to PHASE 1 at 25% health."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["The Nine"] =
	{
		["Looking For Raid"] = {
            "- ALL PHASES:",
			"- Dodge lines on the ground, spread out with circles on players, soak the blue swirlies, soak the small purple circles, and move the bosses out of the big bubble.",
			"- PHASE 1:",
			"- TANKS: Swap on Kyra at as low stacks of [350202] as possible.",
			"- Interrupt Signe's [350287].",
			"- Interrupt and kill the add that spawns.",
			"- Run away from Kyra when she pulls you in and run towards Signe when she pushes you away (get in the bubble).",
			"- Damage the two bosses at the same rate.",
			"- PHASE 2: (Kyra or Signe reaches 15% health)",
			"- DPS should focus on finishing off Kyra and Signe before moving on to Skyja.",
			"- TANKS: swap on Skyja at as low stacks of [350475] as possible.",
			"- If you get the [350542] debuff, move off to the side of the raid with anyone else that has the debuff. Move away once you get dispelled by a healer.",
			"- HEALERS: Dispell [350542] once everyone who has it is grouped up. It'll jump to the nearest player until only one player them all. Then it can be dispelled completely."
		},
		["Normal"] = {
            "- ALL PHASES:",
			"- Dodge lines on the ground, spread out with circles on players, soak the blue swirlies, soak the small purple circles, and move the bosses out of the big bubble.",
			"- PHASE 1:",
			"- TANKS: Swap on Kyra at as low stacks of [350202] as possible.",
			"- Interrupt Signe's [350287].",
			"- Interrupt and kill the add that spawns.",
			"- Run away from Kyra when she pulls you in and run towards Signe when she pushes you away (get in the bubble).",
			"- Damage the two bosses at the same rate.",
			"- PHASE 2: (Kyra or Signe reaches 15% health)",
			"- DPS should focus on finishing off Kyra and Signe before moving on to Skyja.",
			"- TANKS: swap on Skyja at as low stacks of [350475] as possible.",
			"- If you get the [350542] debuff, move off to the side of the raid with anyone else that has the debuff. Move away once you get dispelled by a healer.",
			"- HEALERS: Dispell [350542] once everyone who has it is grouped up. It'll jump to the nearest player until only one player them all. Then it can be dispelled completely."
		},
		["Heroic"] = {
            "- ALL PHASES:",
			"- Dodge lines on the ground, spread out with circles on players, soak the blue swirlies, soak the small purple circles, and move the bosses out of the big bubble.",
			"- PHASE 1:",
			"- TANKS: Swap on Kyra at as low stacks of [350202] as possible.",
			"- Interrupt Signe's [350287].",
			"- Interrupt and kill the add that spawns.",
			"- Run away from Kyra when she pulls you in and run towards Signe when she pushes you away (get in the bubble).",
			"- Damage the two bosses at the same rate.",
			"- PHASE 2: (Kyra or Signe reaches 15% health)",
			"- DPS should focus on finishing off Kyra and Signe before moving on to Skyja.",
			"- TANKS: swap on Skyja at as low stacks of [350475] as possible.",
			"- If you get the [350542] debuff, move off to the side of the raid with anyone else that has the debuff. Move away once you get dispelled by a healer.",
			"- HEALERS: Dispell [350542] once everyone who has it is grouped up. It'll jump to the nearest player until only one player them all. Then it can be dispelled completely.",
			"- Players with [350482] should avoid helping with any soak mechanics."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Remnant of Ner'zhul"] =
	{
		["Looking For Raid"] = {
			"- TANKS: hit the orbs with the frontal cone from the boss and swap after every [349890].",
			"- Everyone should focus down the orbs once they've been cracked out of their shell by the boss's frontal cone. Once the orb has been killed, it needs to be picked up and tossed off the edge.",
            "- Players with the debuff [350469] should move to the edge of the platform, but make sure you're also FACING THE EDGE.",
			"- HEALERS: Dispell players with the debuff [350469] once they've reached the edge and they're facing the edge. They'll get knocked back towards where their back is facing.",
			"- Dodge the swirlies and cones on the ground."
		},
		["Normal"] = {
			"- TANKS: hit the orbs with the frontal cone from the boss and swap after every [349890].",
			"- Everyone should focus down the orbs once they've been cracked out of their shell by the boss's frontal cone. Once the orb has been killed, it needs to be picked up and tossed off the edge.",
            "- Players with the debuff [350469] should move to the edge of the platform, but make sure you're also FACING THE EDGE.",
			"- HEALERS: Dispell players with the debuff [350469] once they've reached the edge and they're facing the edge. They'll get knocked back towards where their back is facing.",
			"- Dodge the swirlies and cones on the ground."
		},
		["Heroic"] = {
			"- TANKS: hit the orbs with the frontal cone from the boss and swap after every [349890].",
			"- Everyone should focus down the orbs once they've been cracked out of their shell by the boss's frontal cone. Once the orb has been killed, it needs to be picked up and tossed off the edge.",
            "- Players with the debuff [350469] should move to the edge of the platform, but make sure you're also FACING THE EDGE.",
			"- HEALERS: Dispell players with the debuff [350469] once they've reached the edge and they're facing the edge. They'll get knocked back towards where their back is facing.",
			"- Dodge the swirlies and cones on the ground."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Soulrender Dormazain"] =
	{
		["Looking For Raid"] = {
			"- TANKS: Swap after every [350422] cast.",
            "- Dodge the big red cones on the ground.",
			"- Interrupt and kill the caster adds that spawn towards the back of the room before they reach Garrosh.",
			"- If you have the [350648] debuff (red circle), stack on adds to make them take more damage.",
			"- Break each set of 3 shackles when they spawn near Garrosh. Use raid cooldowns to survive breaking them and break them one at a time, letting the raid heal up inbetween each."
		},
		["Normal"] = {
			"- TANKS: Swap after every [350422] cast.",
            "- Dodge the big red cones on the ground.",
			"- Interrupt and kill the caster adds that spawn towards the back of the room before they reach Garrosh.",
			"- If you have the [350648] debuff (red circle), stack on adds to make them take more damage.",
			"- Break each set of 3 shackles when they spawn near Garrosh. Use raid cooldowns to survive breaking them and break them one at a time, letting the raid heal up inbetween each."
		},
		["Heroic"] = {
			"- TANKS: Swap after every [350422] cast.",
            "- Dodge the big red cones on the ground.",
			"- Interrupt and kill the caster adds that spawn towards the back of the room before they reach Garrosh.",
			"- If you have the [350648] debuff (red circle), stack on adds to make them take more damage.",
			"- Break each set of 3 shackles when they spawn near Garrosh. Use raid cooldowns to survive breaking them and break them one at a time, letting the raid heal up inbetween each.",
			"- Different players have to break each shackle."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Painsmith Raznal"] =
	{
		["Looking For Raid"] = {
            "- PHASE 1:",
			"- TANKS: Run the boss's weapon-throw cast to a corner of the room and taunt swap after every cast of [355568].",
			"- If you get targeted with [355504], spread out with the chains and try not to run into other people.",
			"- During [352052], focus down the middle spiked ball and run through the gap to survive.",
			"- Dodge the spikes when the tank gets hit by the weapon. The pattern will differ depending on the weapon.",
			"- PHASE 2: (70% and 40% health)",
			"- Dodge the waves of spikes coming from the front of the room and also dodge the swirlies on the ground.",
			"- Lust/hero in the final phase 1 (at 40% health, after phase 2 ends)."
		},
		["Normal"] = {
            "- PHASE 1:",
			"- TANKS: Run the boss's weapon-throw cast to a corner of the room and taunt swap after every cast of [355568].",
			"- If you get targeted with [355504], spread out with the chains and try not to run into other people.",
			"- During [352052], focus down the middle spiked ball and run through the gap to survive.",
			"- Dodge the spikes when the tank gets hit by the weapon. The pattern will differ depending on the weapon.",
			"- PHASE 2: (70% and 40% health)",
			"- Dodge the waves of spikes coming from the front of the room and also dodge the swirlies on the ground.",
			"- Lust/hero in the final phase 1 (at 40% health, after phase 2 ends)."
		},
		["Heroic"] = {
            "- PHASE 1:",
			"- TANKS: Run the boss's weapon-throw cast to a corner of the room and taunt swap after every cast of [355568].",
			"- If you get targeted with [355504], spread out with the chains and try not to run into other people.",
			"- During [352052], focus down the middle spiked ball and run through the gap to survive.",
			"- Dodge the spikes when the tank gets hit by the weapon. The pattern will differ depending on the weapon.",
			"- Avoid stepping on [348456] and try to place the traps in a diagonal pattern. One person should run around and step on them, waiting for the 1.5 second debuff to go away before stepping on the next.",
			"- PHASE 2: (70% and 40% health)",
			"- Dodge the waves of spikes coming from the front of the room and also dodge the swirlies on the ground.",
			"- Lust/hero in the final phase 1 (at 40% health, after phase 2 ends).",
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Guardian of the First Ones"] =
	{
		["Looking For Raid"] = {
            "- Lust/hero on pull.",
			"- TANKS: Keep the boss near an energy core and keep the boss faced away from the raid. You don't want the boss's energy to reach 0. The core charges his energy.",
			"- TANKS: Aim [350732] away to get hit alone and taunt swap after each one.",
			"- The raid should stay within the energy core circle when the boss is charging.",
			"- After about 20 seconds of charging, the core will melt down. Get away from the energy core when this happens.",
			"- Dodge the floating orbs. They can be cleared by letting the boss cast one cast of [352538] upon reaching 0 energy, before dragging him to the next core.",
			"- Get out of the group if you get targeted by [356090] (circles). If you're on a core, just go to the edge of the energy core circle. The rest of the group should go to the opposite edge."
		},
		["Normal"] = {
            "- Lust/hero on pull.",
			"- TANKS: Keep the boss near an energy core and keep the boss faced away from the raid. You don't want the boss's energy to reach 0. The core charges his energy.",
			"- TANKS: Aim [350732] away to get hit alone and taunt swap after each one.",
			"- The raid should stay within the energy core circle when the boss is charging.",
			"- After about 20 seconds of charging, the core will melt down. Get away from the energy core when this happens.",
			"- Dodge the floating orbs. They can be cleared by letting the boss cast one cast of [352538] upon reaching 0 energy, before dragging him to the next core.",
			"- Get out of the group if you get targeted by [356090] (circles). If you're on a core, just go to the edge of the energy core circle. The rest of the group should go to the opposite edge."
		},
		["Heroic"] = {
            "- Lust/hero on pull.",
			"- TANKS: Keep the boss near an energy core and keep the boss faced away from the raid. You don't want the boss's energy to reach 0. The core charges his energy.",
			"- TANKS: Aim [350732] away to get hit alone and taunt swap after each one.",
			"- TANKS: Aim [355352] at the group to split the magic damage. Alternate soak groups if it's cast twice.",
			"- The raid should stay within the energy core circle when the boss is charging.",
			"- After about 20 seconds of charging, the core will melt down. Get away from the energy core when this happens.",
			"- Dodge the floating orbs. They can be cleared by letting the boss cast one cast of [352538] upon reaching 0 energy, before dragging him to the next core.",
			"- Get out of the group if you get targeted by [356090] (circles). If you're on a core, just go to the edge of the energy core circle. The rest of the group should go to the opposite edge."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Fatescribe Roh-Kalo"] =
	{
		["Looking For Raid"] = {
            "- PHASE 1:",
			"- TANKS: Use defensive cooldowns when taunt swapping to mitigate [353603]",
			"- TANKS: Taunt swap when the current tank gets a big circle around them. The current tank should then run away from the raid to mitigate damage from [351680].",
			"- After that, the debuffed tank should run away from the add that fixates them while the rest of the raid cleaves it down.",
			"- Dodge the beams from the edges of the room. It mirrors the other side of the room, so also don't stand opposite of the beams.",
			"- Run out of the group if targeted by [350568].",
			"- Save lust/hero for the final Phase 1.",
			"- PHASE 2: (70% and 40% health)",
			"- Move each of the runes on the ground into place. You have 40 seconds.", 
			"- An ODD number of players standing on the rune moves it CLOCKWISE. An EVEN number of players standing on the rune moves it COUNTERCLOCKWISE.",
			"- Interrupt and kill the big two adds that spawn.",
			"- Dodge the gray orbs floating around.",
			"- After the second phase 2 (40% health), you'll get PHASE 1 and PHASE 2 combined for the remainder of the fight."
		},
		["Normal"] = {
            "- PHASE 1:",
			"- TANKS: Use defensive cooldowns when taunt swapping to mitigate [353603]",
			"- TANKS: Taunt swap when the current tank gets a big circle around them. The current tank should then run away from the raid to mitigate damage from [351680].",
			"- After that, the debuffed tank should run away from the add that fixates them while the rest of the raid cleaves it down.",
			"- Dodge the beams from the edges of the room. It mirrors the other side of the room, so also don't stand opposite of the beams.",
			"- Run out of the group if targeted by [350568].",
			"- Save lust/hero for the final Phase 1.",
			"- PHASE 2: (70% and 40% health)",
			"- Move each of the runes on the ground into place. You have 40 seconds.",
			"- An ODD number of players standing on the rune moves it CLOCKWISE. An EVEN number of players standing on the rune moves it COUNTERCLOCKWISE.",
			"- Interrupt and kill the big two adds that spawn.",
			"- Dodge the gray orbs floating around.",
			"- After the second phase 2 (40% health), you'll get PHASE 1 and PHASE 2 combined for the remainder of the fight."
		},
		["Heroic"] = {
            "- PHASE 1:",
			"- TANKS: Use defensive cooldowns when taunt swapping to mitigate [353603]",
			"- TANKS: Taunt swap when the current tank gets a big circle around them. The current tank should then run away from the raid to mitigate damage from [351680].",
			"- After that, the debuffed tank should run away from the add that fixates them while the rest of the raid cleaves it down.",
			"- Dodge the beams from the edges of the room. It mirrors the other side of the room, so also don't stand opposite of the beams.",
			"- Run out of the group if targeted by [350568].",
			"- Save lust/hero for the final Phase 1.",
			"- PHASE 2: (70% and 40% health)",
			"- Move each of the runes on the ground into place. You have 40 seconds. Only randomly marked players can interact with the runes to move them.",
			"- An ODD number of players standing on the rune moves it CLOCKWISE. An EVEN number of players standing on the rune moves it COUNTERCLOCKWISE.",
			"- Interrupt and kill the big two adds that spawn.",
			"- Dodge the gray orbs floating around.",
			"- After the second phase 2 (40% health), you'll get PHASE 1 and PHASE 2 combined for the remainder of the fight."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Kel'Thuzad"] =
	{
		["Looking For Raid"] = {
            "- PHASE 1:",
			"- TANKS: Taunt swap after every [348071] cast.",
			"- Interrupt and kill the soul adds that spawn. The fractured tank needs to run over each of the soul adds after they die.",
			"- Put the silence circles on the soul adds to help interrupt them and slow/kill the small adds that spawn from the circles.",
			"- Stagger killing the Glacial Spike adds to spread out the raid damage they do when they die.",
			"- Group soak [348756] (the really big, blue swirly) and dispel the root after it hits everyone.",
			"- Stay out of the frost patches on the ground during [354206].",
			"- PHASE 2: (When Kel-Thuzad dies)",
			"- Some players need to go into the phylactery to do 33% of the soul remnant's health.",
			"- In the phylactery, dodge the swirlies and frontal cones.",
			"- Outside the phylactery, interrupt and kill the adds.",
			"- PHASE 3: (After 3 cycles of phase 1)",
			"- Lust/hero at the start of this phase.",
			"- Stay out of the ice patches as long as possible.",
			"- Group soak the big swirly (same as in phase 1).",
			"- Deal with the abominations (same as in phase 2)."
		},
		["Normal"] = {
            "- PHASE 1:",
			"- TANKS: Taunt swap after every [348071] cast.",
			"- Interrupt and kill the soul adds that spawn. The fractured tank needs to run over each of the soul adds after they die.",
			"- Put the silence circles on the soul adds to help interrupt them and slow/kill the small adds that spawn from the circles.",
			"- Stagger killing the Glacial Spike adds to spread out the raid damage they do when they die.",
			"- Group soak [348756] (the really big, blue swirly) and dispel the root after it hits everyone.",
			"- Stay out of the frost patches on the ground during [354206].",
			"- PHASE 2: (When Kel-Thuzad dies)",
			"- Some players need to go into the phylactery to do 33% of the soul remnant's health.",
			"- In the phylactery, dodge the swirlies and frontal cones.",
			"- Outside the phylactery, interrupt and kill the adds.",
			"- PHASE 3: (After 3 cycles of phase 1)",
			"- Lust/hero at the start of this phase.",
			"- Stay out of the ice patches as long as possible.",
			"- Group soak the big swirly (same as in phase 1).",
			"- Deal with the abominations (same as in phase 2)."
		},
		["Heroic"] = {
            "- PHASE 1:",
			"- TANKS: Taunt swap after every [348071] cast.",
			"- Interrupt and kill the soul adds that spawn. The fractured tank needs to run over each of the soul adds after they die.",
			"- Put the silence circles on the soul adds to help interrupt them and slow/kill the small adds that spawn from the circles.",
			"- Stagger killing the Glacial Spike adds to spread out the raid damage they do when they die. Wait for the 10 second DOT to expire before killing the next.",
			"- Group soak [348756] (the really big, blue swirly) and dispel the root after it hits everyone.",
			"- Stay out of the frost patches on the ground during [354206].",
			"- PHASE 2: (When Kel-Thuzad dies)",
			"- Some players need to go into the phylactery to do 33% of the soul remnant's health. You can only go once, so different groups will need to go for each of the three times throughout the fight.",
			"- In the phylactery, dodge the swirlies and frontal cones.",
			"- Outside the phylactery, interrupt and get the adds low in health. They will res within 10 seconds of dying (if still in phase 2) and get buffed at the end of the phase. Finish them off after phase 2 completes.",
			"- Outside the phylactery, prioritize interrupting [352144].",
			"- PHASE 3: (After 3 cycles of phase 1)",
			"- Lust/hero at the start of this phase.",
			"- Stay out of the ice patches as long as possible.",
			"- Group soak the big swirly (same as in phase 1).",
			"- Deal with the abominations (same as in phase 2)."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	},
	["Sylvanas Windrunner"] =
	{
		["Looking For Raid"] = {
            "- PHASE 1:",
			"- TANKS: swap after the current tank gets marked with [347609] or gets hit by [352650]. The marked tank needs to move away from the raid until it ends.",
			"- Dodge the big swirlies that spawn arrow adds. If you have a [347807] debuff, move close to an arrow until you get chained. Quickly kill these adds.",
			"- Dodge swirlies and big dark circles.",
			"- At the end of the phase (at 80% health), quickly kill all of the arrows and dodge the lines on the ground.",
			"- Spread out when the boss casts [348109].",
			"- PHASE 2 (80% health):",
			"- Interrupt [355540]. This will likely wipe the raid if not interrupted in time.",
			"- Avoid the dark circles and other abilities similar to those in phase 1.",
			"- Interrupt and kill the adds. Dispel [351117] from Souljudges and taunt swap on the Goliaths [351672] as needed.",
			"- PHASE 3: (At the end of the chase)",
			"- TANKS: Off-tank needs to run over the pools on the ground, move to a corner of the platform, and get dispelled to move those pools to a better place.",
			"- TANKS: Taunt swap after every [353969] and the previous tank should move to a corner of the platform and get dispelled.",
			"- Spread out and try to be remain at high health to deal with [353955].",
			"- If you get marked with [347609] (purple arrow), jump to another platform and let it expire.",
			"- Jump to another platform before the boss finishes casting [354147], which will destroy the platform she's on."
		},
		["Normal"] = {
            "- PHASE 1:",
			"- TANKS: swap after the current tank gets marked with [347609] or gets hit by [352650]. The marked tank needs to move away from the raid until it ends.",
			"- Dodge the big swirlies that spawn arrow adds. If you have a [347807] debuff, move close to an arrow until you get chained. Quickly kill these adds.",
			"- Dodge swirlies and big dark circles.",
			"- At the end of the phase (at 80% health), quickly kill all of the arrows and dodge the lines on the ground.",
			"- Spread out when the boss casts [348109].",
			"- PHASE 2 (80% health):",
			"- Interrupt [355540]. This will likely wipe the raid if not interrupted in time.",
			"- Avoid the dark circles and other abilities similar to those in phase 1.",
			"- Interrupt and kill the adds. Dispel [351117] from Souljudges and taunt swap on the Goliaths [351672] as needed.",
			"- PHASE 3: (At the end of the chase)",
			"- TANKS: Off-tank needs to run over the pools on the ground, move to a corner of the platform, and get dispelled to move those pools to a better place.",
			"- TANKS: Taunt swap after every [353969] and the previous tank should move to a corner of the platform and get dispelled.",
			"- Spread out and try to be remain at high health to deal with [353955].",
			"- If you get marked with [347609] (purple arrow), jump to another platform and let it expire.",
			"- Jump to another platform before the boss finishes casting [354147], which will destroy the platform she's on."
		},
		["Heroic"] = {
            "- PHASE 1:",
			"- TANKS: swap after the current tank gets marked with [347609] or gets hit by [352650]. The marked tank needs to move away from the raid until it ends.",
			"- Dodge the big swirlies that spawn arrow adds. If you have a [347807] debuff, move close to an arrow until you get chained. Quickly kill these adds.",
			"- Dodge swirlies and big dark circles.",
			"- At the end of the phase (at 80% health), quickly kill all of the arrows and dodge the lines on the ground.",
			"- Spread out when the boss casts [348109].",
			"- PHASE 2 (80% health):",
			"- Interrupt [355540]. This will likely wipe the raid if not interrupted in time.",
			"- Avoid the dark circles and other abilities similar to those in phase 1.",
			"- Interrupt and kill the adds. Dispel [351117] from Souljudges and taunt swap on the Goliaths [351672] as needed.",
			"- PHASE 3: (At the end of the chase)",
			"- TANKS: Off-tank needs to run over the pools on the ground, move to a corner of the platform, and get dispelled to move those pools to a better place.",
			"- TANKS: Taunt swap after every [353969] and the previous tank should move to a corner of the platform and get dispelled.",
			"- Spread out and try to be remain at high health to deal with [353955].",
			"- If you get marked with [347609] (purple arrow), jump to another platform and let it expire.",
			"- Jump to another platform before the boss finishes casting [354147], which will destroy the platform she's on.",
			"- Make sure all [353929] are dispelled and on the floor when [357692] is cast."
		},
		["Mythic"] = {
			"This fight currently doesn't have a description."
		}
	}
}