-- get the namespace
local _, namespace = ...;

-- raid names must match the instance name in WoW
namespace.raids = {}
namespace.raids.bfa = {}
namespace.raids.shadowlands = {}

-- raids
namespace.mappings[2217] = "Ny'alotha, the Waking City"
namespace.mappings[2296] = "Castle Nathria"
namespace.mappings[2450] = "Sanctum of Domination"

-- bfa nya'lotha raid
-- wrathion
namespace.mappings["Searing Breath"] = 305978
namespace.mappings["Searing Armor"] = 306015
namespace.mappings["Incineration"] = 306111
namespace.mappings["Gale Blast"] = 306289
namespace.mappings["Burning Cataclysm"] = 306735
namespace.mappings["Scales of Wrathion"] = 308682
namespace.mappings["Creeping Madness"] = 313250
-- maut
namespace.mappings["Shadow Wounds"] = 307399
namespace.mappings["Devour Magic"] = 307805
namespace.mappings["Devoured Abyss"] = 307839
namespace.mappings["Stygian Annihilation"] = 308044
namespace.mappings["Dark Offering"] = 308872
namespace.mappings["Obsidian Shatter"] = 305722
namespace.mappings["Forbidden Mana"] = 306290
namespace.mappings["Consumed Magic"] = 306387
-- prophet skitra
namespace.mappings["Shadow Shock"] = 308059
namespace.mappings["Shred Psyche"] = 307937
namespace.mappings["Psychic Outburst"] = 309687
namespace.mappings["Dark Ritual"] = 309657
namespace.mappings["Mindquake"] = 307864
namespace.mappings["Images of Absolution"] = 313239
namespace.mappings["Surging Images"] = 313210
namespace.mappings["Clouded Mind"] = 307784
namespace.mappings["Twisted Mind"] = 307785
-- dark inquisitor xanesh
namespace.mappings["Abyssal Strike"] = 311551
namespace.mappings["Torment"] = 311383
namespace.mappings["Soul Flay"] = 306228
namespace.mappings["Void Ritual"] = 312336
namespace.mappings["Voidwoken"] = 309569
namespace.mappings["Void Orb"] = 314202
namespace.mappings["Terror Wave"] = 316211
-- the hivemind
namespace.mappings["Nullification Blast"] = 307968
namespace.mappings["Echoing Void"] = 307232
namespace.mappings["Spawn Acidic Aqir"] = 313441
namespace.mappings["Mind-Numbing Nova"] = 313652
namespace.mappings["Entropic Echo"] = 313692
-- Shad'har
namespace.mappings["Crush"] = 307476
namespace.mappings["Dissolve"] = 307478
namespace.mappings["Debilitating Spit"] = 307358
namespace.mappings["Living Miasma"] = 306692
namespace.mappings["Tasty Morsel"] = 312099
namespace.mappings["Hungry"] = 312328
namespace.mappings["Umbral Eruption"] = 307945
namespace.mappings["Entropic Buildup"] = 308177
namespace.mappings["Bubbling Overflow"] = 314736
-- drest'agath
namespace.mappings["Volatile Seed"] = 310277
namespace.mappings["Void Infused Ichor"] = 308377
namespace.mappings["Aberrant Regeneration"] = 308373
namespace.mappings["Void Glare"] = 310406
namespace.mappings["Mutterings of Insanity"] = 310358
namespace.mappings["Crushing Slam"] = 310614
namespace.mappings["Acid Splash"] = 310584
namespace.mappings["Throes of Agony"] = 308947
namespace.mappings["Reality Tear"] = 308995
namespace.mappings["Spine Eruption"] = 310078
namespace.mappings["Mind Flay"] = 310552
-- vexiona
namespace.mappings["Encroaching Shadows"] = 307317
namespace.mappings["Gift of the Void"] = 306878
namespace.mappings["Annihilation"] = 309774
namespace.mappings["Void Corruption"] = 307019
namespace.mappings["Heart of Darkness"] = 307639
-- ra-den
namespace.mappings["Nullifying Strike"] = 306819
namespace.mappings["Void Empowered"] = 306733
namespace.mappings["Vita Empowered"] = 306732
namespace.mappings["Instability Exposure"] = 306279
namespace.mappings["Unstable Void"] = 306634
namespace.mappings["Void Collapse"] = 306881
namespace.mappings["Ruin"] = 309852
namespace.mappings["Decaying Strike"] = 313213
namespace.mappings["Void Eruption"] = 310003
namespace.mappings["Charged Bonds"] = 310019
namespace.mappings["Lingering Energies"] = 309755
-- il'gynoth
namespace.mappings["Eye of N'Zoth"] = 309961
namespace.mappings["Corruptor's Gaze"] = 310319
namespace.mappings["Pumping Blood"] = 310788
namespace.mappings["Cursed Blood"] = 311159
namespace.mappings["Recurring Nightmare"] = 312486
-- carapace of n'zoth
namespace.mappings["Black Scar"] = 315954
namespace.mappings["Madness Bomb"] = 306978
namespace.mappings["Adaptive Membrane"] = 316848
namespace.mappings["Occipital Blast"] = 307092
namespace.mappings["Insanity Bomb"] = 306985
-- n'zoth
namespace.mappings["Synaptic Shock"] = 313184
namespace.mappings["Anguish"] = 309991
namespace.mappings["Probe Mind"] = 314889
namespace.mappings["Shattered Ego"] = 312155
namespace.mappings["Mindgrasp"] = 315772
namespace.mappings["Paranoia"] = 309980
namespace.mappings["Corrupted Mind"] = 313400
namespace.mappings["Mindgate"] = 309046
namespace.mappings["Tread Lightly"] = 315709
namespace.mappings["Stupefying Glare"] = 318976
namespace.mappings["Harvest Thoughts"] = 317066
