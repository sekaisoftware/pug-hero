-- get the namespace
local _, namespace = ...;

-- namespace methods

namespace.infoloader = {

	-- load options list dungeons selection
	loadDungeonsSelection = function()
	
		-- TODO: should use saved variable, but not working
		PUGHero_loadDungeons("Battle for Azeroth", namespace.dungeons.bfa)
		PUGHero_loadDungeons("Shadowlands", namespace.dungeons.shadowlands)
		-- add on for each expansion
		
	end,
	
	-- load options list raids selection
	loadRaidsSelection = function()
	
		-- TODO: should use saved variable, but not working
		PUGHero_loadRaids("Battle for Azeroth", namespace.raids.bfa)
		PUGHero_loadRaids("Shadowlands", namespace.raids.shadowlands)
		-- add on for each expansion

	end,

}

-- load dungeons to options list
function PUGHero_loadDungeons(expansion, dungeons)

	-- create expansion frame
	local expansionFrame = CreateFrame("Frame");
	expansionFrame.name = expansion;
	expansionFrame.childrenFrames = {};
	
	-- create dungeon frames for each dungeon in the expansion
	for dungeon,bosses in pairs(dungeons) do  		
		local dungeonFrame = CreateFrame("FRAME");
		dungeonFrame.name = dungeon;
		dungeonFrame.parent = expansion;
		dungeonFrame.parentFrame = expansionFrame;
		dungeonFrame.childrenFrames = {};
		table.insert(expansionFrame.childrenFrames, dungeonFrame);
		
		-- create boss frames for each boss in the dungeon
		for boss,difficulty in pairs(bosses) do
			local bossFrame = CreateFrame("FRAME");
			bossFrame.name = boss;
			bossFrame.parent = dungeon;
			bossFrame.parentFrame = dungeonFrame;
			bossFrame.expansion = expansion;
			table.insert(dungeonFrame.childrenFrames, bossFrame);
		end
	end
	
	-- add the expansion frame to the options list
	PUGHeroOptions_AddDungeonCategory(expansionFrame)
	-- add the dungeons for that expansion to the options list
	for i,dungeonFrame in ipairs(expansionFrame.childrenFrames) do
		PUGHeroOptions_AddDungeonCategory(dungeonFrame);
		-- add boss frames for that dungeon to the options list
		for i,bossFrame in ipairs(dungeonFrame.childrenFrames) do
			PUGHeroOptions_AddDungeonCategory(bossFrame);
		end
	end

end

-- load raids to options list
function PUGHero_loadRaids(expansion, raids)

	-- create expansion frame
	local expansionFrame = CreateFrame("Frame");
	expansionFrame.name = expansion;
	expansionFrame.childrenFrames = {};
	
	-- create raid frames for each raid in the expansion
	for raid,bosses in pairs(raids) do  		
		local raidFrame = CreateFrame("FRAME");
		raidFrame.name = raid;
		raidFrame.parent = expansion;
		raidFrame.parentFrame = expansionFrame;
		raidFrame.childrenFrames = {};
		table.insert(expansionFrame.childrenFrames, raidFrame);
		
		-- create boss frames for each boss in the raid
		for boss,difficulty in pairs(bosses) do
			local bossFrame = CreateFrame("FRAME");
			bossFrame.name = boss;
			bossFrame.parent = raid;
			bossFrame.parentFrame = raidFrame;
			bossFrame.expansion = expansion;
			table.insert(raidFrame.childrenFrames, bossFrame);
		end
	end
	
	-- add the expansion frame to the options list
	PUGHeroOptions_AddRaidCategory(expansionFrame)
	-- add the raids for that expansion to the options list
	for i,raidFrame in ipairs(expansionFrame.childrenFrames) do
		PUGHeroOptions_AddRaidCategory(raidFrame);
		-- add boss frames for that raid to the options list
		for i,bossFrame in ipairs(raidFrame.childrenFrames) do
			PUGHeroOptions_AddRaidCategory(bossFrame);
		end
	end


end
