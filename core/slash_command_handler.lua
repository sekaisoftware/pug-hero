-- get the namespace
local _, namespace = ...;

namespace.commands = {

    ["help"] = function() PUGHero_printSlashCommandHelp() end,

    ["options"] = function() PUGHero_openAddonOptions() end,

}

-- handle the slash command /ph
local function PUGHero_handleSlashCommand(arg)

    local bosses = namespace.currentInstance.bosses
    local instanceDifficulty = namespace.currentInstance.difficulty

    -- put args in lower case for any string options
    arg = string.lower(arg)

    -- figure out if a valid boss option was selected
    local selectedBoss = nil
    local bossDifficulty = nil
    if (bosses and instanceDifficulty) then
        -- otherwise, see if we have a fight to explain
        
        local bossIndex = tonumber(arg)
        if (bossIndex) then
            -- iterate through to get the selected boss fight to explain
            local num = 1
            for boss,difficulty in pairs(bosses) do
                if (num == bossIndex) then
                    selectedBoss = boss
                    bossDifficulty = difficulty
                end
                num = num + 1
            end 
        end
    end

    -- handle command line input
    if (arg == "options") then
        -- open options
        namespace.commands.options();
    elseif (arg == "toggle") then
        -- show or hide icon
        PUGHero_toggleMinimapIcon();
    elseif(selectedBoss and bossDifficulty) then
        -- explain the fight if we found the selected boss and instructions
        namespace.chat.printInstructions(selectedBoss, instanceDifficulty, bossDifficulty[instanceDifficulty], namespace.PUGHeroDB.chatChannel)
    elseif (#arg == 0 or arg == "help") then
        -- otherwise, show help
        namespace.commands.help();
    else
        -- invalid selection
        print("Invalid command: " .. arg)
        print("Type |cff00cc66/ph|r or |cff00cc66/ph help|r for a list of slash commands.")
    end

end

function namespace:initSlashCommands(event, name)

    if (name ~= "PUG_Hero") then return end

    -- register the slash commands
    SLASH_PUGHero1 = "/ph"
    SlashCmdList.PUGHero = PUGHero_handleSlashCommand

end


function PUGHero_printSlashCommandHelp()
    local name = namespace.currentInstance.name
    local instanceDifficulty = namespace.currentInstance.difficulty
    local bosses = namespace.currentInstance.bosses
    print(" ")
    print("General Slash Commands:")
    print("|cff00cc66/ph options|r - shows options menu")
    print("|cff00cc66/ph toggle|r - shows or hides the minimap button icon")
    print("|cff00cc66/ph help|r - prints help for slash commands")
    print(" ")
    if (name and bosses and instanceDifficulty) then
        print("Slash Commands to explain fights for |cffFFF569" .. namespace.currentInstance.name .. "|r --- |cffFFF569" .. namespace.currentInstance.difficulty .. "|r")
        local num = 1
        for boss,difficulty in pairs(bosses) do
            print("|cff00cc66/ph " .. num .. "|r - " .. boss)
            num = num + 1
		end 
    else
        print("Not in a supported instance. Other slash commands will become available when in a supported instance.")
    end
   

end

local events = CreateFrame("Frame")
events:RegisterEvent("ADDON_LOADED")
events:SetScript("OnEvent", namespace.initSlashCommands)