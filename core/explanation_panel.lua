-- get the namespace
local _, namespace = ...;

-- default selected difficulty to normal
local selectedDifficulty = "Normal";
-- other selections we want to store off so other buttons/tabs can interact with the current selection
local selectedExpansion = nil;
local selectedInstance = nil;
local selectedBoss = nil;
-- type of selection corresponding to the category tabs, Dungeons or Raids
local selectedInstanceType = "Dungeons"

-- namespace methods
namespace.optionsexplanationpanel = {

	-- update the options explanation panel information
	update = function(frame, instanceType)
		
		selectedInstanceType = instanceType
		selectedExpansion = frame.element.expansion
		selectedInstance = frame.element.parent
		selectedBoss = frame.element.name
		
		-- if we were on LFR in raids, move to normal for dungeons
		if (selectedInstanceType == "Dungeons" and selectedDifficulty == "Looking For Raid") then
			PUGHeroOptionsFrameLFRTab:SetPoint("BOTTOMLEFT", "$parentPanelContainer", "TOPLEFT", "0", "-4")
			PUGHeroOptionsFrameNormalTab:SetPoint("TOPLEFT", "$parentLFRTab", "TOPRIGHT", "-16", "4")
			PUGHeroOptionsFrameHeroicTab:SetPoint("TOPLEFT", "$parentNormalTab", "TOPRIGHT", "-16", "-4")
			PUGHeroOptionsFrameMythicTab:SetPoint("TOPLEFT", "$parentHeroicTab", "TOPRIGHT", "-16", "0")
			selectedDifficulty = "Normal"
		end
		
		-- show everything
		-- container
		PUGHeroOptionsFramePanelContainer:Show();
		-- edit box scroll frame
		PUGHeroOptionsFrameScrollFrame:Show();
		-- edit box background
		PUGHeroOptionsFrameTextBackground:Show();
		-- save button
		PUGHeroOptionsFramePanelContainerSave:Show();
		-- print to self
		PUGHeroOptionsFramePanelContainerExplainFight:Show();
		-- defaults
		PUGHeroOptionsFramePanelContainerDefaults:Show();
		PUGHeroOptionsFramePanelContainerDefaults:SetText("Restore Defaults for " .. selectedBoss)
		-- tabs
		PUGHeroOptionsFrameLFRTab:Show();
		if (selectedInstanceType == "Dungeons") then
			PUGHeroOptionsFrameLFRTab:Hide();
		end
		PUGHeroOptionsFrameNormalTab:Show();
		PUGHeroOptionsFrameHeroicTab:Show();
		PUGHeroOptionsFrameMythicTab:Show();	
		
		-- boss name
		PUGHeroOptionsFramePanelContainerBoss:SetText(selectedBoss)
		PUGHeroOptionsFramePanelContainerBoss:Show()
		-- instance title
		PUGHeroOptionsFramePanelContainerInstance:SetText(selectedInstance)
		PUGHeroOptionsFramePanelContainerInstance:Show()
		-- difficulty
		PUGHeroOptionsFramePanelContainerDifficulty:SetText(selectedDifficulty .. " Difficulty")
		PUGHeroOptionsFramePanelContainerDifficulty:Show()
		
		-- Explanation text
		PUGHero_fillExplanationText(selectedExpansion, selectedInstance, selectedBoss, selectedDifficulty)
		
	end,

}

-- fill the explanation text box
function PUGHero_fillExplanationText(expansion, instance, boss, difficulty)

	explanation = PUGHero_getExplanation(selectedInstanceType, expansion, instance, boss, difficulty)
	
	-- display it
	local text = ""
	for i, line in ipairs(explanation) do
		text = text .. line .. "\n\n"
	end
	PUGHeroOptionsFrameScrollFrameText:SetText(text)

end

-- tabs control
function PUGHeroExplanationPanel_LFRTab_OnClick()

	-- update display to emphasize selected tab
	PUGHeroOptionsFrameLFRTab:SetPoint("BOTTOMLEFT", "$parentPanelContainer", "TOPLEFT", "0", "0")
	PUGHeroOptionsFrameNormalTab:SetPoint("TOPLEFT", "$parentLFRTab", "TOPRIGHT", "-16", "-4")
	PUGHeroOptionsFrameHeroicTab:SetPoint("TOPLEFT", "$parentNormalTab", "TOPRIGHT", "-16", "0")
	PUGHeroOptionsFrameMythicTab:SetPoint("TOPLEFT", "$parentHeroicTab", "TOPRIGHT", "-16", "0")
	
	-- set selected difficulty. Don't set it if we're already on it.
	if (selectedDifficulty ~= "Looking For Raid") then
		selectedDifficulty = "Looking For Raid"
		PUGHeroOptionsFramePanelContainerDifficulty:SetText(selectedDifficulty .. " Difficulty")
		PUGHero_fillExplanationText(selectedExpansion, selectedInstance, selectedBoss, selectedDifficulty)
	end
	
end

function PUGHeroExplanationPanel_NormalTab_OnClick()

	-- update display to emphasize selected tab
	PUGHeroOptionsFrameLFRTab:SetPoint("BOTTOMLEFT", "$parentPanelContainer", "TOPLEFT", "0", "-4")
	PUGHeroOptionsFrameNormalTab:SetPoint("TOPLEFT", "$parentLFRTab", "TOPRIGHT", "-16", "4")
	PUGHeroOptionsFrameHeroicTab:SetPoint("TOPLEFT", "$parentNormalTab", "TOPRIGHT", "-16", "-4")
	PUGHeroOptionsFrameMythicTab:SetPoint("TOPLEFT", "$parentHeroicTab", "TOPRIGHT", "-16", "0")
	
	-- set selected difficulty. Don't set it if we're already on it.
	if (selectedDifficulty ~= "Normal") then
		selectedDifficulty = "Normal"
		PUGHeroOptionsFramePanelContainerDifficulty:SetText(selectedDifficulty .. " Difficulty")
		PUGHero_fillExplanationText(selectedExpansion, selectedInstance, selectedBoss, selectedDifficulty)
	end
	
end

function PUGHeroExplanationPanel_HeroicTab_OnClick()
	
	-- update display to emphasize selected tab
	PUGHeroOptionsFrameLFRTab:SetPoint("BOTTOMLEFT", "$parentPanelContainer", "TOPLEFT", "0", "-4")
	PUGHeroOptionsFrameNormalTab:SetPoint("TOPLEFT", "$parentLFRTab", "TOPRIGHT", "-16", "0")
	PUGHeroOptionsFrameHeroicTab:SetPoint("TOPLEFT", "$parentNormalTab", "TOPRIGHT", "-16", "4")
	PUGHeroOptionsFrameMythicTab:SetPoint("TOPLEFT", "$parentHeroicTab", "TOPRIGHT", "-16", "-4")
	
	-- set selected difficulty. Don't set it if we're already on it.
	if (selectedDifficulty ~= "Heroic") then
		selectedDifficulty = "Heroic"
		PUGHeroOptionsFramePanelContainerDifficulty:SetText(selectedDifficulty .. " Difficulty")
		PUGHero_fillExplanationText(selectedExpansion, selectedInstance, selectedBoss, selectedDifficulty)
	end
	
end

function PUGHeroExplanationPanel_MythicTab_OnClick()
	
	-- update display to emphasize selected tab
	PUGHeroOptionsFrameLFRTab:SetPoint("BOTTOMLEFT", "$parentPanelContainer", "TOPLEFT", "0", "-4")
	PUGHeroOptionsFrameNormalTab:SetPoint("TOPLEFT", "$parentLFRTab", "TOPRIGHT", "-16", "0")
	PUGHeroOptionsFrameHeroicTab:SetPoint("TOPLEFT", "$parentNormalTab", "TOPRIGHT", "-16", "0")
	PUGHeroOptionsFrameMythicTab:SetPoint("TOPLEFT", "$parentHeroicTab", "TOPRIGHT", "-16", "4")
	
	-- set selected difficulty. Don't set it if we're already on it.
	if (selectedDifficulty ~= "Mythic") then
		selectedDifficulty = "Mythic"
		PUGHeroOptionsFramePanelContainerDifficulty:SetText(selectedDifficulty .. " Difficulty")
		PUGHero_fillExplanationText(selectedExpansion, selectedInstance, selectedBoss, selectedDifficulty)
	end
	
end

-- explanation text on changed
function PUGHeroOptionsList_ExplanationText_OnChanged()
	PUGHeroOptionsFrameStatusText:Show()
	PUGHeroOptionsFrameStatusText:SetText("Press the 'Save' button to save your changes.")
end

-- explanation panel save button onclick
function PUGHeroExplanationsPanel_Save_OnClick()

	-- save to saved variables
	local text = PUGHeroOptionsFrameScrollFrameText:GetText()
	
	local bossExplanation = {}
	for line in text:gmatch("[^\r\n]+") do
		table.insert(bossExplanation, line)
	end
	
	PUGHero_setExplanation(selectedInstanceType, selectedExpansion, selectedInstance, selectedBoss, selectedDifficulty, bossExplanation)

	-- Status text
	PUGHeroOptionsFrameStatusText:Show()
	PUGHeroOptionsFrameStatusText:SetText("Changes Saved!")

end

-- explanation panel defaults button onclick
function PUGHeroExplanationsPanel_Defaults_OnClick()
	StaticPopupDialogs["RESTORE_BOSS_DEFAULTS"] = {
		text = "This will restore defaults for the boss '" .. selectedBoss .. "' on all difficulties.\nThis will also reboot the UI.\nContinue?",
		button1 = "Yes",
		button2 = "No",
		OnAccept = function()
			PUGHero_ReloadAndRestoreBossDefaults()
		end,
		timeout = 0,
		whileDead = true,
		hideOnEscape = true,
		preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
	  }
	  
	  StaticPopup_Show("RESTORE_BOSS_DEFAULTS")
end

function PUGHero_ReloadAndRestoreBossDefaults()

	if (PUGHeroDB.dungeons.bfa[selectedInstance]) then
		PUGHeroDB.dungeons.bfa[selectedInstance][selectedBoss] = namespace.dungeons.bfa[selectedInstance][selectedBoss]
	elseif (PUGHeroDB.dungeons.shadowlands[selectedInstance]) then
		PUGHeroDB.dungeons.shadowlands[selectedInstance][selectedBoss] = namespace.dungeons.shadowlands[selectedInstance][selectedBoss]
	end
	ReloadUI()

end

-- explanation panel print to self button onclick
function PUGHeroExplanationsPanel_ExplainFight_OnClick()

	-- get text from the scroll frame
	local text = PUGHeroOptionsFrameScrollFrameText:GetText()

	-- put into table
	local bossExplanation = {}
	for line in text:gmatch("[^\r\n]+") do
		table.insert(bossExplanation, line)
	end
	
	-- print instructions
	namespace.chat.printInstructions(selectedBoss, selectedDifficulty, bossExplanation, "SELF")
	
end











