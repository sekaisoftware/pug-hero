-- get the namespace
local _, namespace = ...;

-- list of selections in the options list
local dungeonsCategories = {};
local raidsCategories = {}
local currentCategories = {};
-- keeps track of elements displayed in the options list
local displayedElements = {}
-- keeps track of the currently selected category tab
local selectedCategoryTab = "Dungeons"

-- secure next function to iterate with
local next = next;
local function SecureNext(elements, key)
	return securecall(next, elements, key);
end

-- tinsert to insert things
local tinsert = tinsert;

-- refresh categories
function PUGHeroOptionsFrame_RefreshCategories ()
	for i, category in ipairs(currentCategories) do 
		pcall(category.refresh, category);
	end
end

-- function that toggles subcategories in the options list
function PUGHeroOptionsListButton_ToggleSubCategories (self)
	local element = self.element;

	element.collapsed = not element.collapsed;
	local collapsed = element.collapsed;

	for i, category in ipairs(currentCategories) do
		if ( category.parent == element.name ) then
			if ( collapsed ) then
				category.hidden = true;
			else
				category.hidden = false;
			end
		end
	end

	PUGHeroCategoryList_Update();
end

-- triggered when a category is clicked in the options list
function PUGHeroOptionsListButton_OnClick (self, mouseButton)
	-- if it's a parent, toggle to show/hide the children
	if ( self.element.hasChildren ) then 
		-- hide/show self
		PUGHeroOptionsListButtonToggle_OnClick(self.toggle);
	else
		-- If we make it here, there are no children.
		-- Display the panel for this category
		local parent = self:GetParent();
		local buttons = parent.buttons;

		OptionsList_ClearSelection(PUGHeroOptionsFrameCategories, PUGHeroOptionsFrameCategories.buttons);

		OptionsList_SelectButton(parent, self);
		
		-- display that info in the explanation panel
		namespace.optionsexplanationpanel.update(self, selectedCategoryTab)

	end

end

-- toggle options list to show or hide
function PUGHeroOptionsListButtonToggle_OnClick (self)
	local button = self:GetParent();
	
	-- check if the children have children and toggle those too
	-- when parent is collapsed, but children are not. Collapse the children as well
	local categoryList = PUGHeroOptionsFrameCategories;
	--local categoryList = categories:GetChildren()
	local children = button.element.childrenFrames;
	-- for each child
	for i, child in ipairs(children) do
		-- check if that child has children and they are open
		if (child.childrenFrames and not child.collapsed) then
			-- for each button in the category list, find the child we need to toggle
			for key, categoryButton in SecureNext, categoryList.buttons do
				if (categoryButton:GetText() == child.name) then
					-- toggle the child that has children expanded
					PUGHeroOptionsListButtonToggle_OnClick(categoryButton.toggle)
				end
			end
		end
	end

	-- toggle the category that was clicked
	button:toggleFunc();
end

-- Triggered on load of options list
function PUGHeroOptionsList_OnLoad (self, buttonTemplate)
	local name = self:GetName();

	--Setup random things!
	self.scrollFrame = _G[name .. "List"];
	self:SetBackdropBorderColor(.6, .6, .6, 1);

	--Create buttons for scrolling
	local buttons = {};
	local button = CreateFrame("BUTTON", name .. "Button1", self, buttonTemplate or "OptionsListButtonTemplate");
	button:SetPoint("TOPLEFT", self, 0, -8);
	self.buttonHeight = button:GetHeight();
	tinsert(buttons, button);

	local maxButtons = (self:GetHeight() - 8) / self.buttonHeight;
	for i = 2, maxButtons do
		button = CreateFrame("BUTTON", name .. "Button" .. i, self, buttonTemplate or "OptionsListButtonTemplate");
		button:SetPoint("TOPLEFT", buttons[#buttons], "BOTTOMLEFT");
		tinsert(buttons, button);
	end

	self.buttonHeight = button:GetHeight();
	self.buttons = buttons;
	
	-- load categories
	namespace.infoloader.loadDungeonsSelection();
	namespace.infoloader.loadRaidsSelection();
	-- default current categories to dungeons
	currentCategories = dungeonsCategories;
	
end

-- Updates the Category List
function PUGHeroCategoryList_Update()

	--Redraw the scroll lists
	local offset = FauxScrollFrame_GetOffset(PUGHeroOptionsFrameCategoriesList);
	local buttons = PUGHeroOptionsFrameCategories.buttons;
	local element;

	for i, element in SecureNext, displayedElements do
		displayedElements[i] = nil;
		displayedElements[i] = nil;
	end

	for i, element in SecureNext, currentCategories do
		if ( not element.hidden ) then
			tinsert(displayedElements, element);
		end
	end

	local numButtons = #buttons;
	local numCategories = #displayedElements;

	if ( numCategories > numButtons and ( not PUGHeroOptionsFrameCategoriesList:IsShown() ) ) then
		OptionsList_DisplayScrollBar(PUGHeroOptionsFrameCategories);
	elseif ( numCategories <= numButtons and ( PUGHeroOptionsFrameCategoriesList:IsShown() ) ) then
		OptionsList_HideScrollBar(PUGHeroOptionsFrameCategories);
	end

	FauxScrollFrame_Update(PUGHeroOptionsFrameCategoriesList, numCategories, numButtons, buttons[1]:GetHeight());

	local selection = PUGHeroOptionsFrameCategories.selection;
	if ( selection ) then
		-- Store the currently selected element and clear all the buttons, we're redrawing.
		OptionsList_ClearSelection(PUGHeroOptionsFrameCategories, PUGHeroOptionsFrameCategories.buttons);
	end

	for i = 1, numButtons do
		element = displayedElements[i + offset];
		if ( not element ) then
			OptionsList_HideButton(buttons[i]);
		else
			PUGHeroOptionsList_DisplayButton(buttons[i], element);

			if ( selection ) and ( selection == element ) and ( not PUGHeroOptionsFrameCategories.selection ) then
				OptionsList_SelectButton(PUGHeroOptionsFrameCategories, buttons[i]);
			end
		end

	end

	if ( selection ) then
		-- If there was a selected element before we cleared the button highlights, restore it, 'cause we're done.
		-- Note: This theoretically might already have been done by OptionsList_SelectButton, but in the event that the selected button hasn't been drawn, this is still necessary.
		PUGHeroOptionsFrameCategories.selection = selection;
	end
end

-- Display buttons in options list (taken from OptionsList_DisplayButton) - need to pull more from this file. Maybe put them somewhere else?
-- supports a list nested 3 deep
function PUGHeroOptionsList_DisplayButton (button, element)
	-- Do display things
	button:Show();
	button.element = element;

	-- third deep
	if (element.parentFrame and element.parentFrame.parent) then
		button:SetNormalFontObject(GameFontHighlightSmall);
		button:SetHighlightFontObject(GameFontHighlightSmall);
		button.text:SetPoint("LEFT", 32, 2);
	-- second deep
	elseif (element.parent) then
		button:SetNormalFontObject(GameFontHighlight);
		button:SetHighlightFontObject(GameFontHighlight);
		button.text:SetPoint("LEFT", 16, 2);
	-- top element
	else
		button:SetNormalFontObject(GameFontNormal);
		button:SetHighlightFontObject(GameFontHighlight);
		button.text:SetPoint("LEFT", 8, 2);
	end
	button.text:SetText(element.name);

	if (element.hasChildren) then
		if (element.collapsed) then
			button.toggle:SetNormalTexture("Interface\\Buttons\\UI-PlusButton-UP");
			button.toggle:SetPushedTexture("Interface\\Buttons\\UI-PlusButton-DOWN");
		else
			button.toggle:SetNormalTexture("Interface\\Buttons\\UI-MinusButton-UP");
			button.toggle:SetPushedTexture("Interface\\Buttons\\UI-MinusButton-DOWN");
		end
		button.toggle:Show();
	else
		button.toggle:Hide();
	end
end

-- add dungeon to options list
function PUGHeroOptions_AddDungeonCategory(frame)
	local parent = frame.parent;
	if ( parent ) then
		for i = 1, #dungeonsCategories do
			if ( dungeonsCategories[i].name == parent ) then
				if ( dungeonsCategories[i].hasChildren ) then
					frame.hidden = ( dungeonsCategories[i].collapsed );
				else
					frame.hidden = true;
					dungeonsCategories[i].hasChildren = true;
					dungeonsCategories[i].collapsed = true;
				end
				tinsert(dungeonsCategories, i + 1, frame);
				PUGHeroCategoryList_Update();
				return;
			end
		end
	end

	if ( position ) then
		tinsert(dungeonsCategories, position, frame);
	else
		tinsert(dungeonsCategories, frame);
	end

	PUGHeroCategoryList_Update();
end

-- add raid to options list
function PUGHeroOptions_AddRaidCategory(frame)
	local parent = frame.parent;
	if ( parent ) then
		for i = 1, #raidsCategories do
			if ( raidsCategories[i].name == parent ) then
				if ( raidsCategories[i].hasChildren ) then
					frame.hidden = ( raidsCategories[i].collapsed );
				else
					frame.hidden = true;
					raidsCategories[i].hasChildren = true;
					raidsCategories[i].collapsed = true;
				end
				tinsert(raidsCategories, i + 1, frame);
				PUGHeroCategoryList_Update();
				return;
			end
		end
	end

	if ( position ) then
		tinsert(raidsCategories, position, frame);
	else
		tinsert(raidsCategories, frame);
	end

	PUGHeroCategoryList_Update();
end

-- dungeons tab control
function PUGHeroOptionsList_DungeonsTab_OnClick()

	-- update display to emphasize selected tab
	PUGHeroOptionsFrameDungeonsTab:SetPoint("BOTTOMLEFT", "$parentCategories", "TOPLEFT", "0", "0")
	PUGHeroOptionsFrameRaidsTab:SetPoint("TOPLEFT", "$parentDungeonsTab", "TOPRIGHT", "-16", "-4")
	
	-- update category list
	selectedCategoryTab = "Dungeons"
	currentCategories = dungeonsCategories
	PUGHeroCategoryList_Update()
	
end

-- raids tab control
function PUGHeroOptionsList_RaidsTab_OnClick()

	-- update display to emphasize selected tab
	PUGHeroOptionsFrameDungeonsTab:SetPoint("BOTTOMLEFT", "$parentCategories", "TOPLEFT", "0", "-4")
	PUGHeroOptionsFrameRaidsTab:SetPoint("TOPLEFT", "$parentDungeonsTab", "TOPRIGHT", "-16", "4")
	
	-- update category list
	selectedCategoryTab = "Raids"
	currentCategories = raidsCategories
	PUGHeroCategoryList_Update()
	
end
