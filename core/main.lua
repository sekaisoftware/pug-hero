-- get the namespace
local _, namespace = ...;

-- Create Map Button Start -------------------------------------------------

local ldbObject = {
	type = "data source",
	text = "n/a",
	icon = "Interface\\AddOns\\PUG_HERO\\media\\minimap-icon",
	label = "PUG HERO",
	OnClick = function(self, button)
		PUGHero_openContextList()
	end,
	OnTooltipShow = function(tooltip)
		tooltip:AddLine("PUG Hero");
		tooltip:AddLine(" ");
		tooltip:AddLine("Chat Channel: " .. namespace.PUGHeroDB.chatChannel);
		tooltip:AddLine(" ");
		tooltip:AddLine("Click to open Context Menu");
	end,
};

-- this function is called immediately after the saved variables have been loaded
-- to create the minimap icon
function PUGHero_LoadMinimapIcon()
	LibStub("LibDataBroker-1.1"):NewDataObject("PUGHero", ldbObject);
	LibStub("LibDBIcon-1.0"):Register("PUGHero", ldbObject, namespace.PUGHeroDB.minimapbutton.profile.LDBIconStorage);
end

-- toggle minimap icon
function PUGHero_toggleMinimapIcon()
	-- toggle
	if namespace.PUGHeroDB.minimapbutton.hidden then
		PUGHero_showMinimapIcon()
	else
		PUGHero_hideMinimapIcon()
	end
end

-- hide minimap icon
function PUGHero_hideMinimapIcon() 
	LibStub("LibDBIcon-1.0"):Hide("PUGHero")
	namespace.PUGHeroDB.minimapbutton.hidden = true
end

-- show minimap icon
function PUGHero_showMinimapIcon() 
	LibStub("LibDBIcon-1.0"):Show("PUGHero")
	namespace.PUGHeroDB.minimapbutton.hidden = false
end

-- Create Map Button End  -------------------------------------------------

-- Chat Channel Context List -----------------------------------------------------------

-- Check box handling for chat channel
local selfChecked = false
local sayChecked = false
local yellChecked = false
local partyChecked = false
local instanceChecked = false
local raidChecked = false
local raidWarningChecked = false
local guildChecked = false

-- Checks the appropriate channel that was clicked and reloads the menu.
function PUGHero_checkChatChannelBox()

	-- Set all options to false
	selfChecked = false
	sayChecked = false
	yellChecked = false
	partyChecked = false
	instanceChecked = false
	raidChecked = false
	raidWarningChecked = false
	guildChecked = false
	
	-- Check the correct option
	if (namespace.PUGHeroDB.chatChannel == "SELF") then
		selfChecked = true
	elseif (namespace.PUGHeroDB.chatChannel == "SAY") then
		sayChecked = true
	elseif (namespace.PUGHeroDB.chatChannel == "YELL") then
		yellChecked = true
	elseif (namespace.PUGHeroDB.chatChannel == "PARTY") then
		partyChecked = true
	elseif (namespace.PUGHeroDB.chatChannel == "INSTANCE_CHAT") then
		instanceChecked = true
	elseif (namespace.PUGHeroDB.chatChannel == "RAID") then
		raidChecked = true
	elseif (namespace.PUGHeroDB.chatChannel == "RAID_WARNING") then
		raidWarningChecked = true
	elseif (namespace.PUGHeroDB.chatChannel == "GUILD") then
		guildChecked = true
	else
		-- Error. We shouldn't get here.
	end

end

-- chat channel section of the minimap icon context list
-- done in a function so that we can change this based on the checked variables
function PUGHero_createChatChannelContextList()
	chatChannelContextList = {
		text = "Select Chat Channel", hasArrow = true, notCheckable = true,
			menuList = 
			{
				{ text = "Self", checked = selfChecked, func = function() print("You will be the only one who sees the boss instructions."); namespace.PUGHeroDB.chatChannel = "SELF"; PUGHero_checkChatChannelBox(); PUGHero_reloadContextList(); end },
				{ text = "Say", checked = sayChecked, func = function() print("The boss instructions will output to /say"); namespace.PUGHeroDB.chatChannel = "SAY"; PUGHero_checkChatChannelBox(); PUGHero_reloadContextList(); end },
				{ text = "Yell", checked = yellChecked, func = function() print("The boss instructions will output to /yell"); namespace.PUGHeroDB.chatChannel = "YELL"; PUGHero_checkChatChannelBox(); PUGHero_reloadContextList(); end },
				{ text = "Party", checked = partyChecked, func = function() print("The boss instructions will output to /party"); namespace.PUGHeroDB.chatChannel = "PARTY"; PUGHero_checkChatChannelBox(); PUGHero_reloadContextList(); end },
				{ text = "Instance", checked = instanceChecked, func = function() print("The boss instructions will output to /i"); namespace.PUGHeroDB.chatChannel = "INSTANCE_CHAT"; PUGHero_checkChatChannelBox(); PUGHero_reloadContextList(); end },
				{ text = "Raid", checked = raidChecked, func = function() print("The boss instructions will output to /raid"); namespace.PUGHeroDB.chatChannel = "RAID"; PUGHero_checkChatChannelBox(); PUGHero_reloadContextList(); end },
				{ text = "Raid Warning", checked = raidWarningChecked, func = function() print("The boss instructions will output to /rw"); namespace.PUGHeroDB.chatChannel = "RAID_WARNING"; PUGHero_checkChatChannelBox(); PUGHero_reloadContextList(); end },
				{ text = "Guild", checked = guildChecked, func = function() print("The boss instructions will output to /g"); namespace.PUGHeroDB.chatChannel = "GUILD"; PUGHero_checkChatChannelBox(); PUGHero_reloadContextList(); end }
			}
	}
	return chatChannelContextList
end

-- end Chat Channel Context List -----------------------------------------------------------

-- Addon Configuration

function PUGHero_openAddonOptions()
	namespace.options.show()
end

-- end Addon Configuration

-- General Context Menu -----------------------------------------------------------

-- Context Menu
local menu = {}
local menuFrame = CreateFrame("Frame", "PUGHeroFrame", UIParent, "UIDropDownMenuTemplate")

-- Clears the menu items completely
function PUGHero_clearContextMenu()
	menu = nil
	menu = {}
end

-- Load the appropriate context menu items for the map button
function PUGHero_loadContextMenu()
	PUGHero_fillContextMenu(namespace.currentInstance.name, namespace.currentInstance.difficulty, namespace.currentInstance.bosses)
end

-- Fills the menu given the input parameters
function PUGHero_fillContextMenu(name, difficultyName, bosses)

	-- Instance Information
	-- in instance and instance is supported
	if (name and difficultyName and bosses) then
		table.insert(menu, { text = name .. " - " .. difficultyName, isTitle = true, notCheckable = true })
		-- Bosses
		for boss,difficulties in pairs(bosses) do  
			table.insert(menu, 
			{ 
				text = "Explain Fight: " .. boss, 
				notCheckable = true,
				func = function() namespace.chat.printInstructions(boss, difficultyName,difficulties[difficultyName], namespace.PUGHeroDB.chatChannel); end 
			})
		end
	-- in instance but instance is not supported
	elseif (name and difficultyName and not bosses) then
		table.insert(menu, { text = name .. " - " .. difficultyName, isTitle = true, notCheckable = true })
		table.insert(menu, { text = "Instance not supported", isTitle = true, notCheckable = true })
	-- not in instance
	else
		table.insert(menu, { text = "Not in supported instance", isTitle = true, notCheckable = true })
	end
	
	-- Other information
	-- Blank Space
	table.insert(menu, { text = "", notCheckable = true, notClickable = true })
	-- Chat channel title
	table.insert(menu, { text = "Chat Channel: " .. namespace.PUGHeroDB.chatChannel, isTitle = true, notCheckable = true })
	-- Select Chat Channel sublist
	table.insert(menu, PUGHero_createChatChannelContextList())
	-- Blank Space
	table.insert(menu, { text = "", notCheckable = true, notClickable = true })
	-- Configuration title
	table.insert(menu, { text = "Configuration", isTitle = true, notCheckable = true })
	-- Options
	table.insert(menu, { text = "Options", func = function() PUGHero_openAddonOptions() end, notCheckable = true })
	-- Blank Space
	table.insert(menu, { text = "", notCheckable = true, notClickable = true })
	-- Close
	table.insert(menu, { text = "Close", notCheckable = true })	
end

-- Called when the map button is clicked
function PUGHero_openContextList()
	EasyMenu(menu, menuFrame, "cursor", 0 , 0, "MENU");
end

-- reload the context menu after some changes, such as checking a chat channel box
function PUGHero_reloadContextList()
	PUGHero_loadCurrentInstanceInfo()
	PUGHero_clearContextMenu()
	PUGHero_loadContextMenu()
	PUGHero_openContextList()
end


-- end General Context Menu -----------------------------------------------------------

-- Other ------------------------------------------------------------------------------

namespace.currentInstance = {
	["name"] = nil,
	["bosses"] = nil,
	["difficulty"] = nil
}

-- General loading of instance info into the namespace for use other places around the addon
function PUGHero_loadCurrentInstanceInfo()

	local inInstance, instanceType = IsInInstance()
	
	-- get the matching dungeon from the namespace and fill the context menu
	local name, typeOfInstance, difficulty, difficultyName, _, _, _, instanceMapId, _ = GetInstanceInfo()
	local bosses = nil

	-- if the name exists in our mappings, map it
	if (namespace.mappings[instanceMapId]) then
		name = namespace.mappings[instanceMapId]
	end
	-- same with difficulty
	if (namespace.mappings[difficulty]) then
		difficultyName = namespace.mappings[difficulty]
	end
	
	-- uncomment for debugging ---------
	-- inInstance = true
	-- name = "De Other Side"
	-- difficulty = 'Normal'
	-- instanceType = "party"
	--
	-- name = "Castle Nathria"
	-- difficulty = 'Heroic'
	-- instanceType = "raid"
	------------------------------------

	-- dungeons
	if (inInstance and (instanceType == "party")) then
		
		-- TODO - figure out a better way of dealing with expansion
		bosses = namespace.PUGHeroDB.dungeons.bfa[name];
		if (not bosses) then
			bosses = namespace.PUGHeroDB.dungeons.shadowlands[name];
		end
		
	-- raids
	elseif (inInstance and (instanceType == "raid")) then

		-- TODO - figure out a better way of dealing with expansion
		bosses = namespace.PUGHeroDB.raids.bfa[name];
		if (not bosses) then
			bosses = namespace.PUGHeroDB.raids.shadowlands[name];
		end

	end
	
	-- neither or instance is not supported
	if (not bosses) then
		name = nil
		bosses = nil
		difficulty = nil
	end

	-- set current instance info in namespace
	namespace.currentInstance.name = name
	namespace.currentInstance.bosses = bosses
	namespace.currentInstance.difficulty = difficultyName

end

-- end Other ---------------------------------------------------------

-- Events ------------------------------------------------------------

-- load context menu when addon has been loaded
local f = CreateFrame("Frame")
 
local function onevent(self, event, arg1, ...)
	-- UPDATE_INSTANCE_INFO
	if (event == "UPDATE_INSTANCE_INFO") then
		PUGHero_checkChatChannelBox()
		PUGHero_loadCurrentInstanceInfo()
		PUGHero_clearContextMenu()
        PUGHero_loadContextMenu()
	end
	
	-- PLAYER_ENTERING_WORLD 
	-- ADDON_LOADED wasn't working with saved vars? Should look into this more.
	if (event == "PLAYER_ENTERING_WORLD") then
		-- hide/show the minimap icon based on our saved variable
		if namespace.PUGHeroDB.minimapbutton.hidden then
			PUGHero_hideMinimapIcon()
		else
			PUGHero_showMinimapIcon()
		end
	end
end
 
f:RegisterEvent("UPDATE_INSTANCE_INFO")
f:RegisterEvent("PLAYER_ENTERING_WORLD")
f:SetScript("OnEvent", onevent)

-- end Events ----------------------------------------------------------