-- get the namespace
local _, namespace = ...;

local frame = CreateFrame("Frame")

-- Messages the selected channel with instructions based on the boss's name and boss table information
namespace.chat = {
	
	-- function that prints instructions using the current chat channel
	printInstructions = function(boss, difficulty, instructions, chatChannel)
	
		-- set print header
		messageHeader = "Boss: " .. boss .. " --- Difficulty: " .. difficulty  
		
		-- print to self if SELF is checked
		if (chatChannel == "SELF") then
			print(messageHeader)
			for i, line in ipairs(instructions) do 
				line = namespace.chat.insertSpellLinks(line)
				print(line)
			end
		-- otherwise say it in the appropriate chat channel
		else
			SendChatMessage(messageHeader, chatChannel, "Common", nil)
			for i, line in ipairs(instructions) do 
				line = namespace.chat.insertSpellLinks(line)
				SendChatMessage(line, chatChannel, "Common", nil) 
			end
		end

	end,
	
	-- function that inserts spell links into a line of the instructions
	insertSpellLinks = function(line)
		
		-- replace any spells that we have mappings for with links to that spell
		result = string.gsub(line, "%[(.-)%]", 
			function(spellName)
			
				-- CASE 1: see if the spell name is actually the spell ID
				local link = GetSpellLink(spellName)
				if (link) then
					return link
				end
			
				-- CASE 2: check if we can get the spellId from our mappings
				local spellId = namespace.mappings[spellName]
				if (spellId) then
					local link = GetSpellLink(spellId)
					return link
				end
				
				-- CASE 3: if we didn't find a matching spell id, keep it the same
				return "[" .. spellName .. "]"
			end
		)

		return result
	
	end,
	
}